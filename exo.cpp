
#include <iostream>
using namespace std;

#include <chrono>

#include <ExoLP/SolverInterface.hpp>
#include <ExoLP/LPV1.hpp>




int main() {
    exo_lp::ProblemMatrices pm;
    exo_lp::Solution sol;
    exo_lp::LPMatrices lpm;
    lpm<<pm;
    if (not lpm.check_finite()){
        lpm.print();
        assert(false);
    }

    auto start_s0 = std::chrono::steady_clock::now();
    exo_lp::SolverInterface* interface0 = exo_lp::newFlopCppSolver(pm);
    interface0->setup();
    auto duration_s0 = std::chrono::duration_cast< chrono::microseconds> 
        (std::chrono::steady_clock::now() - start_s0);

    auto start_s1 = std::chrono::steady_clock::now();
    exo_lp::SolverInterface* interface1 = exo_lp::newLowLevelSolver(pm);
    interface1->setup();
    auto duration_s1 = std::chrono::duration_cast< chrono::microseconds> 
        (std::chrono::steady_clock::now() - start_s1);


    auto start_s2 = std::chrono::steady_clock::now();
    exo_lp::SolverInterface* interface2 = exo_lp::newLowLevelSolver(pm);
    interface2->setup();
    auto duration_s2 = std::chrono::duration_cast< chrono::microseconds> 
        (std::chrono::steady_clock::now() - start_s2);

    auto start_s3 = std::chrono::steady_clock::now();
    exo_lp::SolverInterface* interface3 = exo_lp::newProblemV1(pm);
    interface3->setup();
    auto duration_s3 = std::chrono::duration_cast< chrono::microseconds> 
        (std::chrono::steady_clock::now() - start_s3);

    auto start_s_4_ = std::chrono::steady_clock::now();
    exo_lp::LPV1 lpv1__4_;
    lpv1__4_.com.lower<<lpm.b0;
    lpv1__4_.com.upper<<lpm.b0;
    lpv1__4_.com_by_foot1f<<lpm.S01;
    lpv1__4_.com_by_foot2f<<lpm.S02;

    lpv1__4_.foot1m.lower<<lpm.b1;
    lpv1__4_.foot1m.upper<<lpm.b1;
    lpv1__4_.foot1m_by_tau_a<<lpm.S10;
    lpv1__4_.foot1m_by_foot1f<<lpm.S11;
    lpv1__4_.foot1m_by_foot2f<<lpm.S12;

    lpv1__4_.foot2m.lower<<lpm.b2;
    lpv1__4_.foot2m.upper<<lpm.b2;
    lpv1__4_.foot2m_by_tau_a<<lpm.S20;
    lpv1__4_.foot2m_by_foot1f<<lpm.S21;
    lpv1__4_.foot2m_by_foot2f<<lpm.S22;

    lpv1__4_.solve();


    auto duration_s_4_ = std::chrono::duration_cast< chrono::microseconds> 
        (std::chrono::steady_clock::now() - start_s_4_);

    int N=1000;


    auto start_n3 = std::chrono::steady_clock::now();
    for (int ii=0;ii<N; ii++)
    {
        exo_lp::prof7.start();
        double normalize = 1.0/(0.01+ii*.01+1./(0.01+ii*.01));
        lpm.gamma_1<<normalize*(0.01+ii*.01);// this influences how long it takes to solve.
        lpm.gamma_2<<-normalize*1./(0.01+ii*.01);
        interface3->solve(lpm, sol);
        exo_lp::prof7.stop();
    }
    auto duration_n3 = std::chrono::duration_cast< chrono::microseconds> 
        (std::chrono::steady_clock::now() - start_n3);


    auto start_n_4_ = std::chrono::steady_clock::now();
    for (int ii=0;ii<N; ii++)
    {
        double normalize = 1.0/(0.01+ii*.01+1./(0.01+ii*.01));
        lpv1__4_.foot_equality_gamma1<<normalize*(0.01+ii*.01);// this influences how long it takes to solve.
        lpv1__4_.foot_equality_gamma2<<-normalize*1./(0.01+ii*.01);
        lpv1__4_.solve();
    }
    auto duration_n_4_ = std::chrono::duration_cast< chrono::microseconds> 
        (std::chrono::steady_clock::now() - start_n_4_);

    // auto start_n0 = std::chrono::steady_clock::now();
    // for (int ii=0;ii<N; ii++)
    // {
    //     double normalize = 1.0/(0.01+ii*.01+1./(0.01+ii*.01));
    //     pm.gamma_1=normalize*(0.01+ii*.01);// this influences how long it takes to solve.
    //     pm.gamma_2=normalize*1./(0.01+ii*.01);
    //     interface0->solve(pm, sol);
    // }
    // auto duration_n0 = std::chrono::duration_cast< chrono::microseconds> 
    //     (std::chrono::steady_clock::now() - start_n0);

    // auto start_n1 = std::chrono::steady_clock::now();
    // for (int ii=0;ii<N; ii++)
    // {
    //     double normalize = 1.0/(0.01+ii*.01+1./(0.01+ii*.01));
    //     pm.gamma_1=normalize*(0.01+ii*.01);// this influences how long it takes to solve.
    //     pm.gamma_2=normalize*1./(0.01+ii*.01);
    //     interface1->solve(pm, sol);
    // }
    // auto duration_n1 = std::chrono::duration_cast< chrono::microseconds> 
    //     (std::chrono::steady_clock::now() - start_n1);

    // auto start_n2 = std::chrono::steady_clock::now();
    // for (int ii=0;ii<N; ii++)
    // {
    //     double normalize = 1.0/(0.01+ii*.01+1./(0.01+ii*.01));
    //     lpm.gamma_1<<normalize*(0.01+ii*.01);// this influences how long it takes to solve.
    //     lpm.gamma_2<<-normalize*1./(0.01+ii*.01);
    //     interface2->solve(lpm, sol);
    // }
    // auto duration_n2 = std::chrono::duration_cast< chrono::microseconds> 
    //     (std::chrono::steady_clock::now() - start_n2);


    // cout<<"\n\n<<<<<<<<<<<<<<<<<< FLOP CPP <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n";
    // interface0->print();
    // cout<<"setup "<<" took "<<duration_s0.count()<<" microseconds." <<std::endl; 
    // cout<<"solving "<<N<<" times took "<<duration_n0.count()<<" microseconds." <<std::endl; 
    // cout<<"each solution took "<<duration_n0.count()*(1.0e-6/N)<<" seconds." <<std::endl; 
    // cout<<"Test exo passed."<<endl;

    // cout<<"\n\n<<<<<<<<<<<<<<<<<< SIMPLEX <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n";
    // interface1->print();
    // cout<<"setup "<<" took "<<duration_s1.count()<<" microseconds." <<std::endl; 
    // cout<<"solving "<<N<<" times took "<<duration_n1.count()<<" microseconds." <<std::endl; 
    // cout<<"each solution took "<<duration_n1.count()*(1.0e-6/N)<<" seconds." <<std::endl; 
    // cout<<"Test exo passed."<<endl;

    // cout<<"\n\n<<<<<<<<<<<<<<<<<< SIMPLEX 2 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n";
    // interface2->print();
    // cout<<"setup "<<" took "<<duration_s2.count()<<" microseconds." <<std::endl; 
    // cout<<"solving "<<N<<" times took "<<duration_n2.count()<<" microseconds." <<std::endl; 
    // cout<<"each solution took "<<duration_n2.count()*(1.0e-6/N)<<" seconds." <<std::endl; 
    // cout<<"Test exo passed."<<endl;

    cout<<"\n\n<<<<<<<<<<<<<<<<<< GENERIC <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n";
    interface3->print();
    cout<<"setup "<<" took "<<duration_s3.count()<<" microseconds." <<std::endl; 
    cout<<"solving "<<N<<" times took "<<duration_n3.count()<<" microseconds." <<std::endl; 
    cout<<"each solution took "<<duration_n3.count()*(1.0e-6/N)<<" seconds." <<std::endl; 
    cout<<"Test exo passed."<<endl;

    cout<<"\n\n<<<<<< Profiling <<<<<<<<\n";
    std::cout<<"exo_lp::profiler 1: "<<exo_lp::prof1.average_time_seconds()<<" sec\n";
    std::cout<<"exo_lp::profiler 2: "<<exo_lp::prof2.average_time_seconds()<<" sec\n";
    std::cout<<"exo_lp::profiler 3: "<<exo_lp::prof3.average_time_seconds()<<" sec\n";
    std::cout<<"exo_lp::profiler 4: "<<exo_lp::prof4.average_time_seconds()<<" sec\n";
    std::cout<<"exo_lp::profiler 5: "<<exo_lp::prof5.average_time_seconds()<<" sec\n";
    std::cout<<"exo_lp::profiler 6: "<<exo_lp::prof6.average_time_seconds()<<" sec\n";
    std::cout<<"exo_lp::profiler 7: "<<exo_lp::prof7.average_time_seconds()<<" sec\n";


    cout<<"\n\n<<<<<<<<<<<<<<<<<< GENERIC 2 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n";
    std::cout<<"tau_a: "<<lpv1__4_.tau_a.dat.transpose()<<std::endl;
    std::cout<<"fr_1: "<<lpv1__4_.foot1f.dat.transpose()<<std::endl;
    std::cout<<"fr_2: "<<lpv1__4_.foot2f.dat.transpose()<<std::endl;
    std::cout<<"err_0: "<<lpv1__4_.com.punt_pos.dat.transpose() - lpv1__4_.com.punt_neg.dat.transpose()<<std::endl;
    std::cout<<"err_1: "<<lpv1__4_.foot1m.punt_pos.dat.transpose() - lpv1__4_.foot1m.punt_neg.dat.transpose()<<std::endl;
    std::cout<<"err_2: "<<lpv1__4_.foot2m.punt_pos.dat.transpose() - lpv1__4_.foot2m.punt_neg.dat.transpose()<<std::endl;
    cout<<"setup "<<" took "<<duration_s_4_.count()<<" microseconds." <<std::endl; 
    cout<<"solving "<<N<<" times took "<<duration_n_4_.count()<<" microseconds." <<std::endl; 
    cout<<"each solution took "<<duration_n_4_.count()*(1.0e-6/N)<<" seconds." <<std::endl; 
    cout<<"Test exo passed."<<endl;

    delete interface0;
    delete interface1;
    delete interface2;
    delete interface3;
}
