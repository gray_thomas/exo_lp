#include "LPV1.hpp"
#include <limits>

static const double DBL_MIN = (std::numeric_limits< double >::min)();
static const double DBL_MAX = (std::numeric_limits< double >::max)();
static const double NaN = std::numeric_limits<double>::quiet_NaN();

namespace exo_lp
{

LPV1::LPV1()
: tau_a(8)
, foot1f(6)
, foot2f(6)

, com(6)
, com_by_foot1f(6,6)
, com_by_foot2f(6,6)

, foot1m(6)
, foot1m_by_tau_a(6,8)
, foot1m_by_foot1f(6,6)
, foot1m_by_foot2f(6,6)

, foot2m(6)
, foot2m_by_tau_a(6,8)
, foot2m_by_foot1f(6,6)
, foot2m_by_foot2f(6,6)

, foot_equality(1)
, foot_equality_gamma1(1,1)
, foot_equality_gamma2(1,1)

, foot1f_lim(10)
, foot1f_lim_by_foot1f(10,6)

, foot2f_lim(10)
, foot2f_lim_by_foot2f(10,6)
{
    setup_block_problem();
    gsi = newGenericSolver(this->bp);
}


void LPV1::annotate_model(){

}

void LPV1::solve(){
    // bp.print();
    status = this->gsi->solve();
}


void LPV1::setup_defaults(){
    // columns limits
    tau_a.lower.setConstant(-60); // updates
    tau_a.upper.setConstant(60); // updates
    foot1f.lower << -DBL_MAX*VectorXd::Ones(5), 0;
    foot1f.upper.setConstant(DBL_MAX);
    foot2f.lower << -DBL_MAX*VectorXd::Ones(5), 0;
    foot2f.upper.setConstant(DBL_MAX);
    com.punt_pos.lower.setZero();
    com.punt_pos.upper.setConstant(DBL_MAX);
    com.punt_neg.lower.setZero();
    com.punt_neg.upper.setConstant(DBL_MAX);
    foot1m.punt_pos.lower.setZero();
    foot1m.punt_pos.upper.setConstant(DBL_MAX);
    foot1m.punt_neg.lower.setZero();
    foot1m.punt_neg.upper.setConstant(DBL_MAX);
    foot2m.punt_pos.lower.setZero();
    foot2m.punt_pos.upper.setConstant(DBL_MAX);
    foot2m.punt_neg.lower.setZero();
    foot2m.punt_neg.upper.setConstant(DBL_MAX);

    // row limits
    com.lower.setZero(); // updates
    com.upper.setZero(); // updates
    foot1m.lower.setZero(); // updates
    foot1m.upper.setZero(); // updates
    foot2m.lower.setZero(); // updates
    foot2m.upper.setZero(); // updates
    foot_equality.lower.setZero();
    foot_equality.upper.setZero();
    foot1f_lim.lower.setConstant(-DBL_MAX);
    foot1f_lim.upper.setZero();
    foot2f_lim.lower.setConstant(-DBL_MAX);
    foot2f_lim.upper.setZero();

    // obj
    tau_a.obj.setZero();
    foot1f.obj.setZero();
    foot2f.obj.setZero();
    com.punt_cost << 32, 512, 256, 128, 64, 1028;
    foot1m.punt_cost << 16, 16, 8, 8, 4, 4;
    foot2m.punt_cost << 16, 16, 8, 8, 4, 4;


    // com -- no constants
    // foot1m -- no constants
    // foot2m -- no constants
    // foot_equality
    foot_equality_gamma1.setConstant(.5);
    foot_equality_gamma2.setConstant(-.5);

    // foot1f_lim
    foot1f_lim_by_foot1f << Eigen::MatrixXd::Zero(10,6);
    foot1f_lim_by_foot1f(0,3)=1; foot1f_lim_by_foot1f(0,5)=-.25; // f_1(3) - mu f_1(5)<=0
    foot1f_lim_by_foot1f(1,3)=-1; foot1f_lim_by_foot1f(1,5)=-.25; // -f_1(3) - mu f_1(5)<=0
    foot1f_lim_by_foot1f(2,4)=1; foot1f_lim_by_foot1f(2,5)=-.25; // f_1(4) - mu f_1(5)<=0
    foot1f_lim_by_foot1f(3,4)=-1; foot1f_lim_by_foot1f(3,5)=-.25; // -f_1(4) - mu f_1(5)<=0
    foot1f_lim_by_foot1f(4,2)=1; foot1f_lim_by_foot1f(4,5)=-.05; // f_1(2) - alpha f_1(5)<=0
    foot1f_lim_by_foot1f(5,2)=-1; foot1f_lim_by_foot1f(5,5)=-.05; // -f_1(2) - alpha f_1(5)<=0
    foot1f_lim_by_foot1f(6,1)=1; foot1f_lim_by_foot1f(6,5)=-.025; // f_1(1) - dy f_1(5)<=0
    foot1f_lim_by_foot1f(7,1)=-1; foot1f_lim_by_foot1f(7,5)=-.025; // -f_1(1) - dy f_1(5)<=0
    foot1f_lim_by_foot1f(8,0)=1; foot1f_lim_by_foot1f(8,5)=-.1; // f_1(0) - dx_1_front f_1(5)<=0
    foot1f_lim_by_foot1f(9,0)=-1; foot1f_lim_by_foot1f(9,5)=-.05; // -f_1(0) - dx_1_back f_1(5)<=0

    // foot2f_lim
    foot2f_lim_by_foot2f << foot1f_lim_by_foot1f;

}

void LPV1::setup_block_problem(){
    setup_defaults();

    Eigen::MatrixXd *NEye6 = nmc(-1*Eigen::MatrixXd::Identity(6,6));
    Eigen::MatrixXd *Eye6 = nmc(Eigen::MatrixXd::Identity(6,6));
    Eigen::MatrixXi Eye6mask = Eigen::MatrixXi::Identity(6,6);

    Eigen::MatrixXi foot_lim_mask(10,6);
    foot_lim_mask.setZero();
    foot_lim_mask.block<10,1>(0,5).setOnes();
    foot_lim_mask.block<2,1>(0,3).setOnes();
    foot_lim_mask.block<2,1>(2,4).setOnes();
    foot_lim_mask.block<2,1>(4,2).setOnes();
    foot_lim_mask.block<2,1>(6,1).setOnes();
    foot_lim_mask.block<2,1>(8,0).setOnes();

    Eigen::MatrixXi com_foot_mask=Eigen::MatrixXi::Ones(6,6);
    com_foot_mask.block<3,3>(3,0)<<Eigen::MatrixXi::Zero(3,3);

    this->bp.matrix << ((*nbr()) << BE::Zero(6,8) 
        << BE(6,6,&com_by_foot1f, com_foot_mask)<<BE(6,6,&com_by_foot2f, com_foot_mask)
        << BE(6,6,Eye6,Eye6mask)<< BE(6,6,NEye6,Eye6mask)<<BE::Zero(6,24));

    this->bp.matrix << ((*nbr()) << BE(&foot1m_by_tau_a) << BE(&foot1m_by_foot1f) << BE(&foot1m_by_foot2f)
        <<BE::Zero(6,12) << BE(6,6,Eye6,Eye6mask)<< BE(6,6,NEye6,Eye6mask)<<BE::Zero(6,12));

    this->bp.matrix << ((*nbr()) << BE(&foot2m_by_tau_a) << BE(&foot2m_by_foot1f) << BE(&foot2m_by_foot2f)<<BE::Zero(6,24)
        << BE(6,6,Eye6,Eye6mask)<< BE(6,6,NEye6,Eye6mask));

    this->bp.matrix << ((*nbr()) << BE::Zero(1,8)
        << BE::Zero(1,5) << BE(&foot_equality_gamma1)
        << BE::Zero(1,5) << BE(&foot_equality_gamma2) << BE::Zero(1,36));

    this->bp.matrix << ((*nbr())<<BE::Zero(10,8)<<BE(10,6,&foot1f_lim_by_foot1f,foot_lim_mask)<<BE::Zero(10,42));

    this->bp.matrix << ((*nbr())<<BE::Zero(10,8+6)<<BE(10,6,&foot2f_lim_by_foot2f,foot_lim_mask)<<BE::Zero(10,36));

    this->bp.matrix.finish();

    this->bp.objective << &tau_a.obj << & foot1f.obj << &foot2f.obj
        << &com.punt_cost << &com.punt_cost
        << &foot1m.punt_cost << &foot1m.punt_cost
        << &foot2m.punt_cost << &foot2m.punt_cost;
    
    this->bp.colLower << &tau_a.lower << &foot1f.lower << &foot2f.lower 
        << &com.punt_pos.lower << &com.punt_neg.lower 
        << &foot1m.punt_pos.lower << &foot1m.punt_neg.lower
        << &foot2m.punt_pos.lower << &foot2m.punt_neg.lower;

    this->bp.colUpper << &tau_a.upper << &foot1f.upper << &foot2f.upper 
        << &com.punt_pos.upper << &com.punt_neg.upper 
        << &foot1m.punt_pos.upper << &foot1m.punt_neg.upper
        << &foot2m.punt_pos.upper << &foot2m.punt_neg.upper;

    this->bp.rowLower << &com.lower << &foot1m.lower<< &foot2m.lower << &foot_equality.lower 
        << &foot1f_lim.lower << &foot2f_lim.lower;

    this->bp.rowUpper << &com.upper << &foot1m.upper<< &foot2m.upper << &foot_equality.upper 
        << &foot1f_lim.upper << &foot2f_lim.upper;

    this->bp.colPrimalSolution << &tau_a.dat << &foot1f.dat << &foot2f.dat 
        << &com.punt_pos.dat << &com.punt_neg.dat 
        << &foot1m.punt_pos.dat << &foot1m.punt_neg.dat
        << &foot2m.punt_pos.dat << &foot2m.punt_neg.dat;

    this->bp.colDualSolution << &tau_a.dual << &foot1f.dual << &foot2f.dual 
        << &com.punt_pos.dual << &com.punt_neg.dual 
        << &foot1m.punt_pos.dual << &foot1m.punt_neg.dual
        << &foot2m.punt_pos.dual << &foot2m.punt_neg.dual;

    this->bp.rowPrimalSolution << &com.dat << &foot1m.dat<< &foot2m.dat << &foot_equality.dat 
        << &foot1f_lim.dat << &foot2f_lim.dat;

    this->bp.rowDualSolution << &com.dual << &foot1m.dual<< &foot2m.dual << &foot_equality.dual 
        << &foot1f_lim.dual << &foot2f_lim.dual;

    this->bp.check();
}

}