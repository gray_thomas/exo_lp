#ifndef LPV1_HPP
#define LPV1_HPP

#include "BlockMatrixUtils.hpp"
#include "GenericSolverInterface.hpp"
#include "GenericProblem.hpp"

#include <Eigen/Dense>

using namespace Eigen;
namespace exo_lp{

class LPV1: public exo_lp::GenericProblem{
    std::vector<std::string> rownames;
    std::vector<std::string> colnames;

    void setup_block_problem();
    void setup_defaults();
    void annotate_model();

public:
	ColumnElement tau_a;
	ColumnElement foot1f;
	ColumnElement foot2f;

	PuntingRowElement com;
	MatrixXd com_by_foot1f;
	MatrixXd com_by_foot2f;
	
	PuntingRowElement foot1m;
	MatrixXd foot1m_by_tau_a;
	MatrixXd foot1m_by_foot1f;
	MatrixXd foot1m_by_foot2f;

	PuntingRowElement foot2m;
	MatrixXd foot2m_by_tau_a;
	MatrixXd foot2m_by_foot1f;
	MatrixXd foot2m_by_foot2f;

	RowElement foot_equality;
	MatrixXd foot_equality_gamma1;
	MatrixXd foot_equality_gamma2;

	RowElement foot1f_lim;
	MatrixXd foot1f_lim_by_foot1f;

	RowElement foot2f_lim;
	MatrixXd foot2f_lim_by_foot2f;

	int status;

    LPV1();

    void solve();

};


}


#endif