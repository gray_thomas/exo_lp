#ifndef GENERIC_SOLVER_INTERFACE_HPP
#define GENERIC_SOLVER_INTERFACE_HPP
#include "BlockMatrixUtils.hpp"

namespace exo_lp{

class GenericSolverInterface{
public:
    virtual ~GenericSolverInterface(){}
    virtual int solve(int type=0)=0;
    virtual void print()=0;
};
GenericSolverInterface* newGenericSolver(block_mat::BlockProblem &bp);

}

#endif