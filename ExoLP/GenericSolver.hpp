#ifndef GENERIC_SOLVER_HPP
#define GENERIC_SOLVER_HPP
#include "BlockMatrixUtils.hpp"
#include "GenericSolverInterface.hpp"
#include "ClpSimplex.hpp"
#include "CoinHelperFunctions.hpp"

namespace exo_lp{
namespace lowlevel{
using namespace block_mat;
class GenericSolver: public GenericSolverInterface{
    BlockProblem &bp;

    Eigen::VectorXd colPrimalSolution;
    Eigen::VectorXd rowPrimalSolution;
    Eigen::VectorXd colDualSolution;
    Eigen::VectorXd rowDualSolution;

    Eigen::VectorXd objective;
    Eigen::VectorXd colLower;
    Eigen::VectorXd colUpper;
    Eigen::VectorXd rowLower;
    Eigen::VectorXd rowUpper;

    CoinPackedMatrix matrix;


    // Eigen::Matrix<int, 600, 1> rawi;
    // Eigen::Matrix<double, 1000, 1> raw;
    ClpSimplex model;

    void load_problem();
public:
    GenericSolver(BlockProblem& bp);
    virtual int solve(int type=0);
    virtual void print();
};


}
}
#endif//GENERIC_SOLVER_HPP
