#ifndef LOW_LEVEL_SOLVER_HPP
#define LOW_LEVEL_SOLVER_HPP
#include "BlockMatrixUtils.hpp"
#include "GenericSolverInterface.hpp"
#include "GenericProblem.hpp"
#include "SolverInterface.hpp"

namespace exo_lp{
using namespace block_mat;


class ProblemV1: public SolverInterface, GenericProblem{

    LPMatrices m_lpm;
    SolutionX m_sol;

    // sparsity masks
    Eigen::MatrixXi wrench_limit_mask_mat;
    Eigen::MatrixXi COM_jac_mask;


    // constant data
    Eigen::MatrixXd Eye6;
    Eigen::MatrixXd NEye6;
    Eigen::MatrixXi Eye6mask;

    std::vector<std::string> rownames;
    std::vector<std::string> colnames;

    void annotate_model();
    void load_problem();
    void setup_block_problem();
    void update_problem_data();
    void update_problem_data(ProblemMatrices& pm);
    void solve(Solution &sol);
public:
    ProblemV1(ProblemMatrices& pm);
    virtual void setup();
    virtual void solve(LPMatrices &lpm, Solution &sol);
    virtual void solve(ProblemMatrices &pm, Solution &sol); // deprecated
    virtual void print();
};

}
#endif//LOW_LEVEL_SOLVER_HPP
