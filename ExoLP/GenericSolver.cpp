#include "GenericSolver.hpp"

namespace exo_lp{
namespace lowlevel{

using namespace block_mat;
GenericSolver::GenericSolver(BlockProblem &bp)
    : bp(bp)
    , colPrimalSolution(bp.cols)
    , rowPrimalSolution(bp.rows)
    , colDualSolution(bp.cols)
    , rowDualSolution(bp.rows)

    , objective(bp.cols)
    , colLower(bp.cols)
    , colUpper(bp.cols)
    , rowLower(bp.rows)
    , rowUpper(bp.rows)
{
    assert(bp.check());
    this->load_problem();

    this->model.initialSolve();
    if (bp.rownames not_eq NULL and bp.columnnames not_eq NULL)
    {
        model.copyNames(*bp.rownames,*bp.columnnames);
    }
}


void GenericSolver::load_problem()
{
    matrix = CoinPackedMatrix(false, 
        bp.matrix.numberColumns, 
        bp.matrix.numberRows, 
        bp.matrix.numberElements, 
        bp.matrix.data.data(), 
        bp.matrix.columns.data(), 
        bp.matrix.starts.data(), 
        bp.matrix.length.data()
    ); // false for row major

    bp.objective.copy_to(this->objective);
    bp.colLower.copy_to(this->colLower);
    bp.colUpper.copy_to(this->colUpper);
    bp.rowLower.copy_to(this->rowLower);
    bp.rowUpper.copy_to(this->rowUpper);

    // // load problem
    model.loadProblem(matrix, 
        this->colLower.data(), this->colUpper.data(), 
        this->objective.data(),
        this->rowLower.data(), this->rowUpper.data());
}

int GenericSolver::solve(int type)
{

    bp.matrix.refresh_data();
    this->load_problem();

    switch(type){
        case(0): this->model.dual(0b0, 0b0111); break;
        case(1): this->model.primal(0b0, 0b0111); break;
        case(2): this->model.dual(0b10, 0b0111); break;
        case(3): this->model.primal(0b10, 0b0111); break;
        case(4): this->model.initialSolve(); break;
        case(5): this->model.initialDualSolve(); break;
        case(6): this->model.initialPrimalSolve(); break;
        case(7): this->model.initialBarrierSolve(); break;
        case(8): this->model.initialBarrierNoCrossSolve(); break;
        default: break;
    };

    // this->model.dual(0b0, 0b0111);
    this->model.primal(0b0, 0b0111);

    colPrimalSolution << Eigen::Map<Eigen::VectorXd>(this->model.primalColumnSolution(), bp.cols);
    rowPrimalSolution << Eigen::Map<Eigen::VectorXd>(this->model.primalRowSolution(), bp.rows);
    rowDualSolution << Eigen::Map<Eigen::VectorXd>(this->model.dualRowSolution(), bp.rows);
    colDualSolution << Eigen::Map<Eigen::VectorXd>(this->model.dualColumnSolution(), bp.cols);

    bp.colPrimalSolution.copy_from(this->colPrimalSolution);
    bp.rowPrimalSolution.copy_from(this->rowPrimalSolution);
    bp.colDualSolution.copy_from(this->colDualSolution);
    bp.rowDualSolution.copy_from(this->rowDualSolution);

    return this->model.status();
}

void GenericSolver::print()
{
    std::cout<<"iterations: "<<this->model.getIterationCount()<<std::endl;
    std::cout<<"objective value: "<<this->model.objectiveValue()<<std::endl;
    std::cout<<"optimization direction: "<<this->model.optimizationDirection()<<std::endl; // 1== minimize
    std::cout<<"status: "<<this->model.status()<<std::endl; // 0==optimal, 2==dual infeasible
    int i;
    for (i = 0; i < this->model.numberColumns(); i++)
        printf("Column %d (%s) has value %g (in between %g and %g) with reduced cost %g\n", i, this->model.getColumnName(i).c_str(),
            this->model.primalColumnSolution()[i], this->model.columnLower()[i], this->model.columnUpper()[i], this->model.getReducedCost()[i]);

    for (i = 0; i < this->model.numberRows(); i++)
        printf("Row %d (%s) has value %g (in between %g and %g) and costs %g\n", i, this->model.getRowName(i).c_str(),
            this->model.primalRowSolution()[i], this->model.rowLower()[i], this->model.rowUpper()[i], this->model.getRowPrice()[i]);
}

}

GenericSolverInterface* newGenericSolver(block_mat::BlockProblem &bp){
	return new lowlevel::GenericSolver(bp);
}

}