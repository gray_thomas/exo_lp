#include "LPV3.hpp"
#include <limits>

static const double DBL_MIN = (std::numeric_limits< double >::min)();
static const double DBL_MAX = (std::numeric_limits< double >::max)();
static const double NaN = std::numeric_limits<double>::quiet_NaN();

namespace exo_lp
{

LPV3::LPV3()
: tau_a(8)
, monopod(6)

, com(6)
, com_by_monopod(6, 6)

, monopod_motion(6)
, monopod_motion_by_tau_a(6, 8)
, monopod_motion_by_monopod(6, 6)

, foot_spread(6)
, foot_spread_by_tau_a(6, 8)
, foot_spread_by_monopod_test(6, 6)

, monopod_lim(10)
, monopod_lim_by_monopod(10, 6)

, status(-1)
{
    annotate_model();
    setup_block_problem();
    gsi = newGenericSolver(this->bp);
}


void LPV3::annotate_model()
{

}

void LPV3::solve()
{
    // bp.print();
    status = this->gsi->solve();
}

void LPV3::setup_defaults()
{
    // columns limits
    tau_a.lower.setConstant(-60); // updates
    tau_a.upper.setConstant(60); // updates
    monopod.lower << -DBL_MAX*VectorXd::Ones(5), 0;
    monopod.upper.setConstant(DBL_MAX);

    com.punt_pos.lower.setZero();
    com.punt_pos.upper.setConstant(DBL_MAX);
    com.punt_neg.lower.setZero();
    com.punt_neg.upper.setConstant(DBL_MAX);

    foot_spread.punt_pos.lower.setZero();
    foot_spread.punt_pos.upper.setConstant(DBL_MAX);
    foot_spread.punt_neg.lower.setZero();
    foot_spread.punt_neg.upper.setConstant(DBL_MAX);

    monopod_lim.punt_pos.lower.setZero();
    monopod_lim.punt_pos.upper.setConstant(DBL_MAX);
    monopod_lim.punt_neg.lower.setConstant(NaN);// punt_neg unused for inequality.
    monopod_lim.punt_neg.upper.setConstant(NaN);

    // row limits
    com.lower.setZero(); 
    com.upper.setZero(); 
    monopod_motion.lower.setZero(); 
    monopod_motion.upper.setZero(); 
    foot_spread.lower.setZero(); 
    foot_spread.upper.setZero(); 
    monopod_lim.lower.setConstant(-DBL_MAX);
    monopod_lim.upper.setZero();

    // obj
    tau_a.obj.setZero();
    monopod.obj.setZero();
    com.punt_cost.setOnes();
    monopod_lim.punt_cost.setOnes();
    foot_spread.punt_cost.setConstant(100);

    // monopod_lim
    monopod_lim_by_monopod << Eigen::MatrixXd::Zero(10,6);
    // X, Y force limits
    monopod_lim_by_monopod(0,3)=1; monopod_lim_by_monopod(0,5)=-.25;    //  f_1(3) - mu f_1(5) <= 0 + punt
    monopod_lim_by_monopod(1,3)=-1; monopod_lim_by_monopod(1,5)=-.25;   // -f_1(3) - mu f_1(5) <= 0 + punt
    monopod_lim_by_monopod(2,4)=1; monopod_lim_by_monopod(2,5)=-.25;    //  f_1(4) - mu f_1(5) <= 0 + punt
    monopod_lim_by_monopod(3,4)=-1; monopod_lim_by_monopod(3,5)=-.25;   // -f_1(4) - mu f_1(5) <= 0 + punt

    // Z torque limits
    monopod_lim_by_monopod(4,2)=1; monopod_lim_by_monopod(4,5)=-.05;    //  f_1(2) - alpha f_1(5) <= 0 + punt
    monopod_lim_by_monopod(5,2)=-1; monopod_lim_by_monopod(5,5)=-.05;   // -f_1(2) - alpha f_1(5) <= 0 + punt

    // COP geometry limitations (currently set to have COP ~= virtual monopod)
    monopod_lim_by_monopod(6,0)=0.01; monopod_lim_by_monopod(6,1)=1; monopod_lim_by_monopod(6,5)=-1e-4;   //  f_1(1) - dy f_1(5) <= 0 + punt
    monopod_lim_by_monopod(7,0)=-0.01; monopod_lim_by_monopod(7,1)=-1; monopod_lim_by_monopod(7,5)=-1e-4;  // -f_1(1) - dy f_1(5) <= 0 + punt
    monopod_lim_by_monopod(8,0)=1; monopod_lim_by_monopod(8,1)=0.01; monopod_lim_by_monopod(8,5)=-1e-4;     //  f_1(0) - dx_1_front f_1(5) <= 0 + punt
    monopod_lim_by_monopod(9,0)=-1; monopod_lim_by_monopod(9,1)=-0.01; monopod_lim_by_monopod(9,5)=-1e-4;   // -f_1(0) - dx_1_back f_1(5) <= 0 + punt

}

void LPV3::setup_block_problem(){
    setup_defaults();

    Eigen::MatrixXd *NEye6 = nmc(-1*Eigen::MatrixXd::Identity(6,6));
    Eigen::MatrixXd *Eye6 = nmc(Eigen::MatrixXd::Identity(6,6));
    Eigen::MatrixXi Eye6mask = Eigen::MatrixXi::Identity(6,6);

    Eigen::MatrixXi monopod_lim_mask(10,6);
    monopod_lim_mask.setZero();
    monopod_lim_mask.block<10,1>(0,5).setOnes();
    monopod_lim_mask.block<2,1>(0,3).setOnes();
    monopod_lim_mask.block<2,1>(2,4).setOnes();
    monopod_lim_mask.block<2,1>(4,2).setOnes();
    monopod_lim_mask.block<2,1>(6,1).setOnes();
    monopod_lim_mask.block<2,1>(8,0).setOnes();
    // allow x-y toruqe constraints that are not axis aligned
    monopod_lim_mask.block<2,1>(6,0).setOnes();
    monopod_lim_mask.block<2,1>(8,1).setOnes();

    Eigen::MatrixXi com_monopod_mask=Eigen::MatrixXi::Ones(6,6);
    com_monopod_mask.block<3,3>(3,0)<<Eigen::MatrixXi::Zero(3,3);

    this->bp.matrix << ((*nbr()) // com task
        << BE::Zero(6,8) // tau_a
        << BE(6,6,&com_by_monopod, com_monopod_mask) // monopod
        << BE(6,6,Eye6,Eye6mask)<< BE(6,6,NEye6,Eye6mask) // com punt
        << BE::Zero(6,12) // foot_sread punt
        << BE::Zero(6,10) // monopod_lim punt
        );

    this->bp.matrix << ((*nbr()) // monopod_motion constraint
        << BE(&monopod_motion_by_tau_a) // tau_a
        << BE(&monopod_motion_by_monopod) // monopod
        << BE::Zero(6,12) // com punt
        << BE::Zero(6,12) // foot_spread punt
        << BE::Zero(6,10) // monopod_lim punt
        );

    this->bp.matrix << ((*nbr()) // foot_spread task
        << BE(&foot_spread_by_tau_a) // tau_a
        << BE(&foot_spread_by_monopod_test) // monopod
        << BE::Zero(6,12) // com punt
        << BE(6,6,Eye6,Eye6mask) << BE(6,6,NEye6,Eye6mask) // foot_spread punt
        << BE::Zero(6,10) // monopod_lim punt
        );

    this->bp.matrix << ((*nbr()) // monopod_lim (inequality) task
        << BE::Zero(10,8) // tau_a
        << BE(&monopod_lim_by_monopod) // monopod
        << BE::Zero(10,12) // com punt
        << BE::Zero(10,12) // foot_spread punt
        << BE(10, 10, nmc(-Eigen::MatrixXd::Identity(10,10)), Eigen::MatrixXi::Identity(10,10)) // monopod_lim punt
        );

    this->bp.matrix.finish();

    RowElement* _rows[]={&com, &monopod_motion, &foot_spread, &monopod_lim};
    RowElement* _cols[]={&tau_a, &monopod, &com.punt_pos, &com.punt_neg,
        &foot_spread.punt_pos, &foot_spread.punt_neg, &monopod_lim.punt_pos};


    std::vector<RowElement*> rows(_rows, _rows + sizeof(_rows)/sizeof(_rows[0]));
    std::vector<RowElement*> cols(_cols, _cols + sizeof(_cols)/sizeof(_cols[0]));


    this->bp.objective
        << &tau_a.obj
        << &monopod.obj
        << &com.punt_cost
        << &com.punt_cost
        << &foot_spread.punt_cost
        << &foot_spread.punt_cost
        << &monopod_lim.punt_cost
        ;

    for (int c=0;c<int(cols.size());c++){
        this->bp.colLower << &cols[c]->lower;
        this->bp.colUpper << &cols[c]->upper;
        this->bp.colPrimalSolution << &cols[c]->dat;
        this->bp.colDualSolution << &cols[c]->dual;
    }

    for (int r=0;r<int(rows.size());r++){
        this->bp.rowLower << &rows[r]->lower;
        this->bp.rowUpper << &rows[r]->upper;
        this->bp.rowPrimalSolution << &rows[r]->dat;
        this->bp.rowDualSolution << &rows[r]->dual;
    }
    this->bp.check();
}


}