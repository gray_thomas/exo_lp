#ifndef GENERIC_PROBLEM_HPP
#define GENERIC_PROBLEM_HPP
#include "BlockMatrixUtils.hpp"
#include "GenericSolverInterface.hpp"
namespace exo_lp{

typedef block_mat::BlockElement BE;

class GenericProblem{
private:
    std::vector<Eigen::VectorXd*> const_vectors;
    std::vector<Eigen::MatrixXd*> const_mats;
    std::vector<block_mat::BlockRow*> block_rows;
protected:
    Eigen::VectorXd* nvc(const Eigen::VectorXd& vec);
    Eigen::MatrixXd* nmc(const Eigen::MatrixXd& vec);
    block_mat::BlockRow* nbr();
    block_mat::BlockProblem bp;
    GenericSolverInterface* gsi;
    GenericProblem();
public:
    ~GenericProblem();
};


struct RowElement{
	VectorXd dat;
	VectorXd upper;
	VectorXd lower;
	VectorXd dual;
	RowElement(int size);
};

struct ColumnElement : public RowElement{
	VectorXd obj;
	ColumnElement(int size);
};

struct PuntColumn : public RowElement{
	PuntColumn(int size);
};

struct PuntingRowElement: public RowElement{
	PuntColumn punt_pos;
	PuntColumn punt_neg;
	VectorXd punt_cost;
	PuntingRowElement(int size);
};

}
#endif