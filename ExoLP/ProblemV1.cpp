#include "ProblemV1.hpp"


const double COIN_DBL_MIN = (std::numeric_limits< double >::min)();
const double COIN_DBL_MAX = (std::numeric_limits< double >::max)();


namespace exo_lp{

using namespace block_mat;


ProblemV1::ProblemV1(ProblemMatrices& pm)
{
    // Only zero these elements once.
    this->m_lpm.E1 << Eigen::MatrixXd::Zero(10,6);
    this->m_lpm.E2 << Eigen::MatrixXd::Zero(10,6);
    // wrench_equalitymat << Eigen::MatrixXd::Zero(1,12);

    this->update_problem_data(pm); // populates this->m_lpm
    // this->update_problem_data(); // This is how we update, btw.


    ////// Setup Sparsity Masks: ///////////////////////
    wrench_limit_mask_mat = Eigen::MatrixXi::Zero(10,6); // assume f1 and f2 are next to eachother.
    assert(wrench_limit_mask_mat.size()==60);
    for (int r=0; r< this->m_lpm.E1.rows(); r++){
        for (int c=0; c<this->m_lpm.E1.cols(); c++){
            if (this->m_lpm.E1(r,c)!=0.0){
                wrench_limit_mask_mat(r,c)=1;
            }
        }
    }

    COM_jac_mask=Eigen::MatrixXi::Ones(6,6);
    COM_jac_mask.block<3,3>(3,0)<<Eigen::MatrixXi::Zero(3,3);

    // Constant data elements.
    Eye6 = Eigen::MatrixXd::Identity(6,6);
    NEye6 = -Eigen::MatrixXd::Identity(6,6);
    Eye6mask = Eigen::MatrixXi::Identity(6,6);


    assert(wrench_limit_mask_mat.size()==60);
    annotate_model();
    this->setup_block_problem();


    gsi = newGenericSolver(this->bp);

}




void ProblemV1::setup_block_problem(){

    this->bp.matrix << ((*nbr()) << BlockElement::Zero(6,8) << BlockElement(6,6,&this->m_lpm.S01, COM_jac_mask)<<BlockElement(6,6,&this->m_lpm.S02, COM_jac_mask)
        << BlockElement(6,6,&Eye6,Eye6mask)<< BlockElement(6,6,&NEye6,Eye6mask)<<BlockElement::Zero(6,24));

    this->bp.matrix << ((*nbr()) << BlockElement(&this->m_lpm.S10) << BlockElement(&this->m_lpm.S11) << BlockElement(&this->m_lpm.S12)<<BlockElement::Zero(6,12)
        << BlockElement(6,6,&Eye6,Eye6mask)<< BlockElement(6,6,&NEye6,Eye6mask)<<BlockElement::Zero(6,12));

    this->bp.matrix << ((*nbr()) << BlockElement(&this->m_lpm.S20) << BlockElement(&this->m_lpm.S21) << BlockElement(&this->m_lpm.S22)<<BlockElement::Zero(6,24)
        << BlockElement(6,6,&Eye6,Eye6mask)<< BlockElement(6,6,&NEye6,Eye6mask));

    this->bp.matrix << ((*nbr()) << BlockElement::Zero(1,8) <<
        BlockElement::Zero(1,5) << BlockElement(&this->m_lpm.gamma_1)
        <<BlockElement::Zero(1,5) << BlockElement(&this->m_lpm.gamma_2) << BlockElement::Zero(1,36));

    this->bp.matrix << ((*nbr())<<BlockElement::Zero(10,8)<<BlockElement(10,6,&this->m_lpm.E1,wrench_limit_mask_mat)<<BlockElement::Zero(10,42));

    this->bp.matrix << ((*nbr())<<BlockElement::Zero(10,8+6)<<BlockElement(10,6,&this->m_lpm.E2,wrench_limit_mask_mat)<<BlockElement::Zero(10,36));

    this->bp.matrix.finish();
    this->bp.objective << nvc(Eigen::VectorXd::Zero(8+6+6));

    this->bp.objective << &this->m_lpm.W0 << &this->m_lpm.W0 
        << &this->m_lpm.W1 << &this->m_lpm.W1
        << &this->m_lpm.W2 << &this->m_lpm.W2;
    
    this->bp.colLower << &this->m_lpm.tau_min <<
        nvc(-COIN_DBL_MAX*Eigen::VectorXd::Ones(5)) << nvc(Eigen::VectorXd::Zero(1)) <<
        nvc(-COIN_DBL_MAX*Eigen::VectorXd::Ones(5)) << nvc(Eigen::VectorXd::Zero(1)) << 
        nvc(Eigen::VectorXd::Zero(36));

    this->bp.colUpper << &this->m_lpm.tau_max << 
        nvc(COIN_DBL_MAX*Eigen::VectorXd::Ones(12)) <<
        nvc(COIN_DBL_MAX*Eigen::VectorXd::Ones(36));

    this->bp.rowLower << &this->m_lpm.b0 << &this->m_lpm.b1 << &this->m_lpm.b2 <<
        nvc(Eigen::VectorXd::Zero(1)) << nvc(-COIN_DBL_MAX*Eigen::VectorXd::Ones(20));

    this->bp.rowUpper << &this->m_lpm.b0 << &this->m_lpm.b1 << &this->m_lpm.b2 <<
        nvc(Eigen::VectorXd::Zero(1)) << nvc(Eigen::VectorXd::Zero(20));

    this->bp.colPrimalSolution 
        << &this->m_sol.tau_a 
        << &this->m_sol.foot_1_wrench
        << &this->m_sol.foot_2_wrench 
        << &this->m_sol.lambda_0_p
        << &this->m_sol.lambda_0_n
        << &this->m_sol.lambda_1_p
        << &this->m_sol.lambda_1_n
        << &this->m_sol.lambda_2_p
        << &this->m_sol.lambda_2_n;

    this->bp.rowPrimalSolution << nvc(Eigen::VectorXd::Zero(19))
        << &this->m_sol.contact_1_slack << &this->m_sol.contact_2_slack;

    this->bp.colDualSolution << nvc(Eigen::VectorXd::Zero(8+6+6+36));
    this->bp.rowDualSolution << nvc(Eigen::VectorXd::Zero(39));

    this->bp.check();

}

void ProblemV1::update_problem_data(ProblemMatrices& pm){
    this->m_lpm.S01 << pm.S_v.transpose()*pm.J_1.transpose();
    this->m_lpm.S02 << pm.S_v.transpose()*pm.J_2.transpose();

    // b0
    static Eigen::VectorXd SvTB_gmat(6);        //, SvTB_gmat(6) // b0
    static Eigen::VectorXd KpcomSvTJc0TFc0mat(6);    //, KpcomSvTJc0TFc0mat(6) // b0
    static Eigen::VectorXd KpcomSvTJc1TFc1mat(6);    //, KpcomSvTJc1TFc1mat(6) // b0
    static Eigen::VectorXd KpcomSvTJc2TFc2mat(6);    //, KpcomSvTJc2TFc2mat(6) // b0

    SvTB_gmat << pm.S_v.transpose()*pm.B_g_vector;
    KpcomSvTJc0TFc0mat << pm.Kp_com * pm.S_v.transpose() * pm.J_C0.transpose() * pm.f_C0;
    KpcomSvTJc1TFc1mat << pm.Kp_com * pm.S_v.transpose() * pm.J_C1.transpose() * pm.f_C1;
    KpcomSvTJc2TFc2mat << pm.Kp_com * pm.S_v.transpose() * pm.J_C2.transpose() * pm.f_C2;

    this->m_lpm.b0 << SvTB_gmat+KpcomSvTJc0TFc0mat 
        + (1-pm.fuzzy_1_contact)*KpcomSvTJc1TFc1mat
        +(1-pm.fuzzy_2_contact)*KpcomSvTJc2TFc2mat;

    //////////////////
    this->m_lpm.S10 << pm.J_1*pm.A_inverse*pm.S_a;
    this->m_lpm.S11 << pm.J_1*pm.A_inverse*pm.J_1.transpose();
    this->m_lpm.S12 << pm.J_1*pm.A_inverse*pm.J_2.transpose();

    // b1
    static Eigen::VectorXd J1dotqdotmat(6); //    , J1dotqdotmat(6) // b1
    static Eigen::VectorXd J1AinvBgmat(6); //    , J1AinvBgmat(6) // b1
    static Eigen::VectorXd J1AinvJc0Tfc0mat(6); //    , J1AinvJc0Tfc0mat(6) // b1
    static Eigen::VectorXd J1AinvJc2Tfc2mat(6); //    , J1AinvJc2Tfc2mat(6) // b1
    static Eigen::VectorXd Kpswing1J1AinvJc1Tfc1mat(6); //    , Kpswing1J1AinvJc1Tfc1mat(6) // b1
    static Eigen::VectorXd Kpswing1J1AinvJc0Tfc0mat(6); //    , Kpswing1J1AinvJc0Tfc0mat(6) // b1

    J1dotqdotmat << Eigen::VectorXd::Zero(6);
    J1AinvBgmat << pm.J_1*pm.A_inverse*pm.B_g_vector;
    J1AinvJc0Tfc0mat << pm.J_1*pm.A_inverse*pm.J_C0.transpose()*pm.f_C0;
    J1AinvJc2Tfc2mat << pm.J_1*pm.A_inverse*pm.J_C2.transpose()*pm.f_C2;
    Kpswing1J1AinvJc1Tfc1mat << pm.Kp_swing_1*pm.J_1*pm.A_inverse*pm.J_C1.transpose()*pm.f_C1;
    Kpswing1J1AinvJc0Tfc0mat << pm.Kp_swing_1*pm.J_1*pm.A_inverse*pm.J_C0.transpose()*pm.f_C0;

    this->m_lpm.b1 << J1AinvBgmat - J1dotqdotmat
        -pm.fuzzy_1_contact*J1AinvJc0Tfc0mat
        -pm.fuzzy_1_contact*J1AinvJc2Tfc2mat
        +(1-pm.fuzzy_1_contact)*Kpswing1J1AinvJc1Tfc1mat
        +(1-pm.fuzzy_1_contact)*Kpswing1J1AinvJc0Tfc0mat;

    //////////////////
    this->m_lpm.S20 << pm.J_2*pm.A_inverse*pm.S_a;
    this->m_lpm.S21 << pm.J_2*pm.A_inverse*pm.J_1.transpose();
    this->m_lpm.S22 << pm.J_2*pm.A_inverse*pm.J_2.transpose();

    // b2
    static Eigen::VectorXd J2dotqdotmat(6);//    , J2dotqdotmat(6) // b2
    static Eigen::VectorXd J2AinvBgmat(6);//    , J2AinvBgmat(6) // b2
    static Eigen::VectorXd J2AinvJc0Tfc0mat(6);//    , J2AinvJc0Tfc0mat(6) // b2
    static Eigen::VectorXd J2AinvJc1Tfc1mat(6);//    , J2AinvJc1Tfc1mat(6) // b2
    static Eigen::VectorXd Kpswing1J2AinvJc2Tfc2mat(6);//    , Kpswing1J2AinvJc2Tfc2mat(6) // b2
    static Eigen::VectorXd Kpswing1J2AinvJc0Tfc0mat(6);//    , Kpswing1J2AinvJc0Tfc0mat(6) // b2

    J2dotqdotmat << Eigen::VectorXd::Zero(6);
    J2AinvBgmat << pm.J_2*pm.A_inverse*pm.B_g_vector;
    J2AinvJc0Tfc0mat << pm.J_2*pm.A_inverse*pm.J_C0.transpose()*pm.f_C0;
    J2AinvJc1Tfc1mat << pm.J_2*pm.A_inverse*pm.J_C1.transpose()*pm.f_C1;
    Kpswing1J2AinvJc2Tfc2mat << pm.Kp_swing_2*pm.J_2*pm.A_inverse*pm.J_C2.transpose()*pm.f_C2;
    Kpswing1J2AinvJc0Tfc0mat << pm.Kp_swing_2*pm.J_2*pm.A_inverse*pm.J_C0.transpose()*pm.f_C0;

    this->m_lpm.b2 << J2AinvBgmat - J2dotqdotmat
        -pm.fuzzy_2_contact*J2AinvJc0Tfc0mat
        -pm.fuzzy_2_contact*J2AinvJc1Tfc1mat        
        +(1-pm.fuzzy_2_contact)*Kpswing1J2AinvJc2Tfc2mat
        +(1-pm.fuzzy_2_contact)*Kpswing1J2AinvJc0Tfc0mat;

    /////////////////
    this->m_lpm.E1(0,3)=1; this->m_lpm.E1(0,5)=-pm.mu; // f_1(3) - mu f_1(5)<=0
    this->m_lpm.E1(1,3)=-1; this->m_lpm.E1(1,5)=-pm.mu; // -f_1(3) - mu f_1(5)<=0
    this->m_lpm.E1(2,4)=1; this->m_lpm.E1(2,5)=-pm.mu; // f_1(4) - mu f_1(5)<=0
    this->m_lpm.E1(3,4)=-1; this->m_lpm.E1(3,5)=-pm.mu; // -f_1(4) - mu f_1(5)<=0
    this->m_lpm.E1(4,2)=1; this->m_lpm.E1(4,5)=-pm.alpha; // f_1(2) - alpha f_1(5)<=0
    this->m_lpm.E1(5,2)=-1; this->m_lpm.E1(5,5)=-pm.alpha; // -f_1(2) - alpha f_1(5)<=0
    this->m_lpm.E1(6,1)=1; this->m_lpm.E1(6,5)=-pm.dy; // f_1(1) - dy f_1(5)<=0
    this->m_lpm.E1(7,1)=-1; this->m_lpm.E1(7,5)=-pm.dy; // -f_1(1) - dy f_1(5)<=0
    this->m_lpm.E1(8,0)=1; this->m_lpm.E1(8,5)=-pm.dx_1_front; // f_1(0) - dx_1_front f_1(5)<=0
    this->m_lpm.E1(9,0)=-1; this->m_lpm.E1(9,5)=-pm.dx_1_back; // -f_1(0) - dx_1_back f_1(5)<=0

    this->m_lpm.E2 << this->m_lpm.E1;
    this->m_lpm.E2(8,5) = -pm.dx_2_front;
    this->m_lpm.E2(9,5) = -pm.dx_2_back;

    this->m_lpm.gamma_1<<pm.gamma_1;
    this->m_lpm.gamma_2<<-pm.gamma_2;

    this->m_lpm.W0<<pm.W_0_data;
    this->m_lpm.W1<<pm.W_1_data;
    this->m_lpm.W2<<pm.W_2_data;


    this->m_lpm.tau_max<<60*Eigen::VectorXd::Ones(8);
    this->m_lpm.tau_min<<-60*Eigen::VectorXd::Ones(8);
}

void ProblemV1::update_problem_data(){
}

void ProblemV1::setup(){
    }

void ProblemV1::solve(ProblemMatrices &pm, Solution &sol)
{
    this->update_problem_data(pm);
    this->solve(sol);
}

void ProblemV1::solve(LPMatrices &lpm, Solution &sol){
        exo_lp::prof6.start();
    this->m_lpm<<lpm;
    this->solve(sol);
        exo_lp::prof6.stop();
}


void ProblemV1::solve( Solution &sol){
    // this->bp.print();
            exo_lp::prof5.start();
        exo_lp::prof3.start();
    this->update_problem_data();
        exo_lp::prof3.stop();

        exo_lp::prof4.start();
    this->m_sol.status = this->gsi->solve();
        exo_lp::prof4.stop();

    this->m_sol >> sol;
        exo_lp::prof5.stop();
}

void ProblemV1::print(){
    Solution sol;
    this->m_sol >> sol;
    std::cout<<"tau_a: "<<sol.tau_a.transpose()<<std::endl;
    std::cout<<"fr_1: "<<sol.foot_1_wrench.transpose()<<std::endl;
    std::cout<<"fr_2: "<<sol.foot_2_wrench.transpose()<<std::endl;
    std::cout<<"err_0: "<<sol.com_err.transpose()<<std::endl;
    std::cout<<"err_1: "<<sol.foot_1_err.transpose()<<std::endl;
    std::cout<<"err_2: "<<sol.foot_2_err.transpose()<<std::endl;
}

void ProblemV1::annotate_model(){


    rownames.push_back("vj x-torque balance (Nm)");
    rownames.push_back("vj y-torque balance (Nm)");
    rownames.push_back("vj z-torque balance (Nm)");
    rownames.push_back("vj x-force balance (N)");
    rownames.push_back("vj y-force balance (N)");
    rownames.push_back("vj z-force balance (N)");

    rownames.push_back("foot_1 x-rot_acc balance (rad/s/s)");
    rownames.push_back("foot_1 y-rot_acc balance (rad/s/s)");
    rownames.push_back("foot_1 z-rot_acc balance (rad/s/s)");
    rownames.push_back("foot_1 x-acc balance (m/s/s)");
    rownames.push_back("foot_1 y-acc balance (m/s/s)");
    rownames.push_back("foot_1 z-acc balance (m/s/s)");

    rownames.push_back("foot_2 x-rot_acc balance (rad/s/s)");
    rownames.push_back("foot_2 y-rot_acc balance (rad/s/s)");
    rownames.push_back("foot_2 z-rot_acc balance (rad/s/s)");
    rownames.push_back("foot_2 x-acc balance (m/s/s)");
    rownames.push_back("foot_2 y-acc balance (m/s/s)");
    rownames.push_back("foot_2 z-acc balance (m/s/s)");

    rownames.push_back("foot z force equality (N)");

    rownames.push_back("foot_1 friction max x-force (N)");
    rownames.push_back("foot_1 friction min x-force (N)");
    rownames.push_back("foot_1 friction max y-force (N)");
    rownames.push_back("foot_1 friction min y-force (N)");
    rownames.push_back("foot_1 friction max z-torque (Nm)");
    rownames.push_back("foot_1 friction min z-torque (Nm)");
    rownames.push_back("foot_1 friction max y-torque (Nm)");
    rownames.push_back("foot_1 friction min y-torque (Nm)");
    rownames.push_back("foot_1 friction max x-torque (Nm)");
    rownames.push_back("foot_1 friction min x-torque (Nm)");

    rownames.push_back("foot_2 friction max x-force (N)");
    rownames.push_back("foot_2 friction min x-force (N)");
    rownames.push_back("foot_2 friction max y-force (N)");
    rownames.push_back("foot_2 friction min y-force (N)");
    rownames.push_back("foot_2 friction max z-torque (Nm)");
    rownames.push_back("foot_2 friction min z-torque (Nm)");
    rownames.push_back("foot_2 friction max y-torque (Nm)");
    rownames.push_back("foot_2 friction min y-torque (Nm)");
    rownames.push_back("foot_2 friction max x-torque (Nm)");
    rownames.push_back("foot_2 friction min x-torque (Nm)");
    this->bp.rownames = &rownames;

    colnames.push_back("tau_a 0: left ad-ab (Nm)");
    colnames.push_back("tau_a 1: left fl-ex (Nm)");
    colnames.push_back("tau_a 2: left knee (Nm)");
    colnames.push_back("tau_a 3: left ank (Nm)");
    colnames.push_back("tau_a 4: right ad-ab (Nm)");
    colnames.push_back("tau_a 5: right fl-ex (Nm)");
    colnames.push_back("tau_a 6: right knee (Nm)");
    colnames.push_back("tau_a 7: right ank (Nm)");

    colnames.push_back("foot_1 x-torque reaction (Nm)");
    colnames.push_back("foot_1 y-torque reaction (Nm)");
    colnames.push_back("foot_1 z-torque reaction (Nm)");
    colnames.push_back("foot_1 x-force reaction (N)");
    colnames.push_back("foot_1 y-force reaction (N)");
    colnames.push_back("foot_1 z-force reaction (N)");

    colnames.push_back("foot_2 x-torque reaction (Nm)");
    colnames.push_back("foot_2 y-torque reaction (Nm)");
    colnames.push_back("foot_2 z-torque reaction (Nm)");
    colnames.push_back("foot_2 x-force reaction (N)");
    colnames.push_back("foot_2 y-force reaction (N)");
    colnames.push_back("foot_2 z-force reaction (N)");

    colnames.push_back("vj x-torque punt_+ (Nm)");
    colnames.push_back("vj y-torque punt_+ (Nm)");
    colnames.push_back("vj z-torque punt_+ (Nm)");
    colnames.push_back("vj x-force punt_+ (N)");
    colnames.push_back("vj y-force punt_+ (N)");
    colnames.push_back("vj z-force punt_+ (N)");
    colnames.push_back("vj x-torque punt_- (Nm)");
    colnames.push_back("vj y-torque punt_- (Nm)");
    colnames.push_back("vj z-torque punt_- (Nm)");
    colnames.push_back("vj x-force punt_- (N)");
    colnames.push_back("vj y-force punt_- (N)");
    colnames.push_back("vj z-force punt_- (N)");

    colnames.push_back("foot_1 x-rot_acc punt_+ (rad/s/s)");
    colnames.push_back("foot_1 y-rot_acc punt_+ (rad/s/s)");
    colnames.push_back("foot_1 z-rot_acc punt_+ (rad/s/s)");
    colnames.push_back("foot_1 x-acc punt_+ (m/s/s)");
    colnames.push_back("foot_1 y-acc punt_+ (m/s/s)");
    colnames.push_back("foot_1 z-acc punt_+ (m/s/s)");
    colnames.push_back("foot_1 x-rot_acc punt_- (rad/s/s)");
    colnames.push_back("foot_1 y-rot_acc punt_- (rad/s/s)");
    colnames.push_back("foot_1 z-rot_acc punt_- (rad/s/s)");
    colnames.push_back("foot_1 x-acc punt_- (m/s/s)");
    colnames.push_back("foot_1 y-acc punt_- (m/s/s)");
    colnames.push_back("foot_1 z-acc punt_- (m/s/s)");

    colnames.push_back("foot_2 x-rot_acc punt_+ (rad/s/s)");
    colnames.push_back("foot_2 y-rot_acc punt_+ (rad/s/s)");
    colnames.push_back("foot_2 z-rot_acc punt_+ (rad/s/s)");
    colnames.push_back("foot_2 x-acc punt_+ (m/s/s)");
    colnames.push_back("foot_2 y-acc punt_+ (m/s/s)");
    colnames.push_back("foot_2 z-acc punt_+ (m/s/s)");
    colnames.push_back("foot_2 x-rot_acc punt_- (rad/s/s)");
    colnames.push_back("foot_2 y-rot_acc punt_- (rad/s/s)");
    colnames.push_back("foot_2 z-rot_acc punt_- (rad/s/s)");
    colnames.push_back("foot_2 x-acc punt_- (m/s/s)");
    colnames.push_back("foot_2 y-acc punt_- (m/s/s)");
    colnames.push_back("foot_2 z-acc punt_- (m/s/s)");
    this->bp.columnnames = &colnames;
}


SolverInterface* newProblemV1(ProblemMatrices &pm){
	return new ProblemV1(pm);
}

}