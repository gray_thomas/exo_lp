#ifndef LPV2_HPP
#define LPV2_HPP

#include "BlockMatrixUtils.hpp"
#include "GenericSolverInterface.hpp"
#include "GenericProblem.hpp"

#include <Eigen/Dense>

using namespace Eigen;
namespace exo_lp{

class LPV2: public exo_lp::GenericProblem{
    std::vector<std::string> rownames;
    std::vector<std::string> colnames;

    void setup_block_problem();
    void setup_defaults();
    void annotate_model();
public:
	ColumnElement tau_a;
	ColumnElement foot1;
	ColumnElement foot2;

	PuntingRowElement com;
	MatrixXd com_by_foot1;
	MatrixXd com_by_foot2;
	
	PuntingRowElement foot_center;
	MatrixXd foot_center_by_tau_a;
	MatrixXd foot_center_by_foot1;
	MatrixXd foot_center_by_foot2;

	PuntingRowElement foot_spread;
	MatrixXd foot_spread_by_tau_a;

	RowElement foot_equality;
	MatrixXd foot_equality_gamma1;
	MatrixXd foot_equality_gamma2;

	RowElement foot1_lim;
	MatrixXd foot1_lim_by_foot1;

	RowElement foot2_lim;
	MatrixXd foot2_lim_by_foot2;

	int status;

    LPV2();

    void solve();

};

}


#endif