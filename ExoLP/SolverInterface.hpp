#ifndef SOLVER_INTERFACE_HPP
#define SOLVER_INTERFACE_HPP
#include <ExoLP/ProblemMatrices.hpp>
#include <ExoLP/BlockMatrixUtils.hpp>


#if defined(__GNUC__) || defined(__clang__)
#define DEPRECATED __attribute__((deprecated))
#elif defined(_MSC_VER)
#define DEPRECATED __declspec(deprecated)
#else
#pragma message("WARNING: You need to implement DEPRECATED for this compiler")
#define DEPRECATED
#endif

namespace exo_lp{

class SolverInterface{
public:
	virtual ~SolverInterface(){};
    virtual void setup()=0;
    virtual DEPRECATED void solve(ProblemMatrices &pm, Solution &sol)=0;
    virtual void solve(LPMatrices &lpm, Solution &sol){};
    virtual void print()=0;
};

SolverInterface* newLowLevelSolver(ProblemMatrices &pm);
SolverInterface* DEPRECATED newFlopCppSolver(ProblemMatrices &pm);
SolverInterface* newProblemV1(ProblemMatrices &pm);
}
#endif //SOLVER_INTERFACE_HPP