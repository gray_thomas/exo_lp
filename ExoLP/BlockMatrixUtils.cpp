#include "BlockMatrixUtils.hpp"

namespace block_mat{
    // convenience constructor
BlockElement::BlockElement(Eigen::MatrixXd* data):
rows(data->rows()), cols(data->cols()), is_zero(false), is_sparse(false), data(data), indexes(rows, cols), col0(-1), row0(-1){
    num_elements = rows*cols;
    indexes = Eigen::MatrixXi::Zero(rows, cols);
    mask = Eigen::MatrixXi::Ones(rows, cols);
}

// dense/zero constructor
BlockElement::BlockElement(int rows, int cols, bool is_zero, Eigen::MatrixXd* data):
rows(rows), cols(cols), is_zero(is_zero), is_sparse(false), data(data), indexes(rows, cols), col0(-1), row0(-1)
{
    num_elements = is_zero?0:rows*cols;
    indexes = Eigen::MatrixXi::Zero(rows,cols);
    mask = Eigen::MatrixXi::Ones(rows,cols); 
}

//sparse (mask) constructor
BlockElement::BlockElement(int rows, int cols, Eigen::MatrixXd* data, Eigen::MatrixXi mask):
rows(rows), cols(cols), is_zero(false), is_sparse(true), data(data), indexes(rows, cols), col0(-1), row0(-1)
{
    // std::cout<<"size of sparse block:"<<mask.sum()<<std::endl;
    indexes = Eigen::MatrixXi::Zero(rows,cols);
    this->mask = Eigen::MatrixXi(mask); // copy constructor
    assert(this->mask.size()==this->data->size());
    num_elements = this->mask.sum();
}

void BlockElement::refresh_data(double * datalist){
    // std::cout<<"indexes="<<indexes<<std::endl;
    if (!is_zero){
        for (int r=0; r<rows; r++){
            for (int c=0; c<cols; c++){
                if (mask(r,c)){
                    datalist[indexes(r,c)]=(*data)(r,c);
                }
            }
        }
    }
}

void BlockElement::refresh_col_indexes(int * columnslist){
    for (int r=0; r<rows; r++){
        for (int c=0; c<cols; c++){
            columnslist[indexes(r,c)]=c+col0;
        }
    }
}

void BlockElement::print(int row){
    for (int c=0; c<cols; c++){
        if (is_zero){
            std::cout<<"-, ";
            continue;
        }
        if (is_sparse){
            if (mask(row,c)==0){
                std::cout<<"-, ";
                continue;
            }
        }
        std::cout<<(*data)(row,c)<<", ";
    }
    std::cout << "; ";
}


BlockRow& BlockRow::operator<<(BlockElement el){
    el.col0=cols;
    cols = cols+el.cols;
    if (rows==-1)rows=el.rows;
    assert (rows==el.rows);
    col_starts.push_back(cols);
    elements.push_back(el);
    bcols++;
    return *this;
}

BlockRow::BlockRow():
    rows(-1),
    bcols(0),
    row0(-1),
    cols(0)
{
    col_starts=std::vector<int>();
    elements=std::vector<BlockElement>();
}
void BlockRow::print(){
    std::cout<<"\t Block row "<< rows <<" "<< cols<<"\n";
    for (int r=0; r<rows; r++){
        std::cout<<"\t\t";
        for (int c=0; c<bcols; c++){
            elements[c].print(r);
        }
        std::cout<<"\n";
    }
}
int BlockRow::num_elements(){
    int num=0;
    for (int j=0;j<bcols;j++)
    {
        num+=elements[j].num_elements;
    }
    return num;
}


BlockMatrix::BlockMatrix():
    cols(-1),
    rows(0),
    brows(0),
    numberColumns(-1),
    numberRows(-1),
    numberElements(-1),
    starts(),
    length(),
    columns(),
    data()
{
    list_rows=std::vector<BlockRow>();
    row_starts=std::vector<int>();
    row_starts.push_back(0);
}
int BlockMatrix::num_elements(){
    int num=0;
    for (int i=0;i<brows;i++){
        num+=list_rows[i].num_elements();
    }
    return num;
}

void BlockMatrix::finish(){
    numberColumns = cols;
    numberRows = rows;
    numberElements = num_elements();
    // std::cout<<"numberElements: "<<numberElements<<std::endl;
    starts=Eigen::Matrix<CoinBigIndex, 1, Dynamic> ::Zero(numberRows+1);
    length=Eigen::Matrix<int, 1, Dynamic>::Zero(numberRows);
    columns=Eigen::Matrix<int, 1, Dynamic>::Zero(numberElements);
    data=Eigen::Matrix<double, 1, Dynamic>::Zero(numberElements);

    setup_structure(data.data(),columns.data(), length.data(), starts.data());
}


void BlockMatrix::refresh_data(){
    for (int br=0; br<brows; br++){
        for (int bc=0; bc<list_rows[br].bcols; bc++){
            if (!list_rows[br].elements[bc].is_zero){
                list_rows[br].elements[bc].refresh_data(data.data());
            }
        }
    }
}

void BlockMatrix::setup_structure(double* data, int* columns, int* length, CoinBigIndex* starts){
    // (column) starts for a row major sparse matrix
    for (int i=0; i<rows; i++){
        starts[i]=-1; // initialization code
    }

    int row=0;
    int ndx=0;


    for (int br=0; br<brows; br++){
        for (int r=0; r<list_rows[br].rows; r++){
            int col=0;
            for (int bc=0; bc<list_rows[br].bcols; bc++){
                if (!list_rows[br].elements[bc].is_zero){
                    for (int c=0; c<list_rows[br].elements[bc].cols; c++){
                        // <row+r, col+c> -- ndx

                        if (list_rows[br].elements[bc].is_sparse) {
                            if (list_rows[br].elements[bc].mask(r,c)==0) {
                                continue; // skip masked elements for sparse block elements.
                            }
                        }
                        list_rows[br].elements[bc].indexes(r,c)=ndx;
                        data[ndx]=(*list_rows[br].elements[bc].data)(r,c);
                        columns[ndx]=col+c;
                        if (starts[row+r]==-1){
                            starts[row+r]=ndx;} // index of the start of a row
                        ndx++;
                    }//after iterating over local columns
                }//after considering the case of non-zero block elements
                col+=list_rows[br].elements[bc].cols;
            }//after iterating over blocks of columns  
            length[row+r]=ndx-starts[row+r]; 
        }//after iterating over local rows
        row+=list_rows[br].rows;
    }//after iterating over blocks of rows

    starts[rows]=ndx;
    assert(ndx==num_elements());
}
void BlockMatrix::print(){
    std::cout<<"matrix, with "<<num_elements()<<" elements\n";
    for (int i=0;i<brows;i++)
    {
        std::cout<<"row\n";
        list_rows[i].print();
    }
    std::cout<<"data:\n"<<data<<std::endl;
    std::cout<<"columns:\n"<<columns<<std::endl;
    std::cout<<"length:\n"<<length<<std::endl;
    std::cout<<"starts:\n"<<starts<<std::endl;
    std::cout<<"/matrix"<<std::endl;
}
BlockMatrix& BlockMatrix::operator<<(BlockRow& row){
    row.row0=row_starts.back();
    if (cols==-1) cols=row.cols;
    assert(cols==row.cols);
    row_starts.push_back(row.row0+row.rows);
    list_rows.push_back(row);
    rows+=row.rows;
    brows++;

    return *this;
}

BlockVector::BlockVector()
: size(0)
{

}

BlockVector& BlockVector::operator<<(Eigen::VectorXd* el){
    this->list_elements.push_back(el);
    this->starts.push_back(this->size);
    this->size+=el->size();
    return *this;
}

void BlockVector::copy_to(Eigen::VectorXd& target){
    assert(target.size()==this->size);
    for (int i=0;i<this->list_elements.size();i++){
        target.segment(this->starts[i],this->list_elements[i]->size())<<*this->list_elements[i];
    }
}

void BlockVector::copy_from(Eigen::VectorXd& target){
    assert(target.size()==this->size);
    for (int i=0;i<this->list_elements.size();i++){
        *this->list_elements[i]<<target.segment(this->starts[i],this->list_elements[i]->size());
    }
}
void BlockVector::print(){
    for (int i=0;i<this->list_elements.size();i++){
        for (int j=0;j<this->list_elements[i]->size();j++)
        {
            std::cout<<(*this->list_elements[i])[j]<<", ";
        }
    }
    std::cout<<std::endl;
}

BlockProblem::BlockProblem()
: rownames(NULL)
, columnnames(NULL)
{

}


void BlockProblem::print(){
    Eigen::VectorXd objective(cols);
    Eigen::VectorXd colLower(cols);
    Eigen::VectorXd colUpper(cols);
    Eigen::VectorXd colPrimalSolution(cols);
    Eigen::VectorXd colDualSolution(cols);
    Eigen::VectorXd rowLower(rows);
    Eigen::VectorXd rowUpper(rows);
    Eigen::VectorXd rowPrimalSolution(rows);
    Eigen::VectorXd rowDualSolution(rows);

    this->objective.copy_to(objective);
    this->colLower.copy_to(colLower);
    this->colUpper.copy_to(colUpper);
    this->colPrimalSolution.copy_to(colPrimalSolution);
    this->colDualSolution.copy_to(colDualSolution);
    this->rowLower.copy_to(rowLower);
    this->rowUpper.copy_to(rowUpper);
    this->rowPrimalSolution.copy_to(rowPrimalSolution);
    this->rowDualSolution.copy_to(rowDualSolution);

    std::cout<<"Block Problem with "<<cols<<" variables and "<<rows<<"constraints.\n";
    std::cout<<"Objective: "<<objective.transpose()<<"\n";
    std::cout<<"colLower: "<<colLower.transpose()<<"\n";
    std::cout<<"colUpper: "<<colUpper.transpose()<<"\n";
    std::cout<<"colPrimalSolution: "<<colPrimalSolution.transpose()<<"\n";
    std::cout<<"colDualSolution: "<<colDualSolution.transpose()<<"\n";

    int cndx = 0;
    for (int i=0;i<matrix.brows;i++)
    {
        std::cout<<"row\n";
        std::cout<<"\t Block row "<< matrix.list_rows[i].rows <<" "<< matrix.list_rows[i].cols<<"\n";
        for (int r=0; r<matrix.list_rows[i].rows; r++){
            std::cout<<"\t\t";
            for (int c=0; c<matrix.list_rows[i].bcols; c++){
                matrix.list_rows[i].elements[c].print(r);
            }
            std::cout<<" :: ---- :: "
            << "Lower " << rowLower(cndx) <<", "
            << "Upper " << rowUpper(cndx) <<", "
            << "PrimalSolution " << rowPrimalSolution(cndx) <<", "
            << "DualSolution " << rowDualSolution(cndx) << std::endl;

            cndx++;
        }
    }
    // std::cout<<"data:\n"<<data<<std::endl;
    // std::cout<<"columns:\n"<<columns<<std::endl;
    // std::cout<<"length:\n"<<length<<std::endl;
    // std::cout<<"starts:\n"<<starts<<std::endl;
    std::cout<<"/problem"<<std::endl;
}

bool BlockProblem::check(){
    assert(matrix.data.size()>0);
    rows=matrix.rows;
    cols=matrix.cols;

    assert(objective.size==cols);
    assert(colLower.size==cols);
    assert(colUpper.size==cols);
    assert(rowLower.size==rows);
    assert(rowUpper.size==rows);

    assert(colPrimalSolution.size==cols);
    assert(rowPrimalSolution.size==rows);
    assert(colDualSolution.size==cols);
    assert(rowDualSolution.size==rows);

    if (rownames not_eq NULL) assert(rownames->size()==rows);
    if (columnnames not_eq NULL) assert(columnnames->size()==cols);

    return true;
}


// typedef Eigen::Matrix<double, Joints::num_joints, Joints::num_joints, Eigen::RowMajor> MassMatrixType;

void test_blocks(){
    BlockMatrix mat;
    BlockRow row0;
    Eigen::MatrixXd I2 = Eigen::MatrixXd::Identity(2,2);
    BlockElement iblock(2, 2, &I2, Eigen::MatrixXi::Identity(2,2));
    // BlockElement iblock(2, 2, false, &I2);
    // BlockElement iblock(2, 2, &I2, Eigen::MatrixXi::Ones(2,2));
    row0<<iblock << BlockElement::Zero(2,2);
    mat<<row0;
    BlockRow row1;
    std::cout<<"testing block row print for index 0:\n";
    row1.print();
    std::cout<<"\n/row"<<std::endl;

    Eigen::MatrixXd tmp(3,4);
    tmp<< 1,2,3,4,5,6,7,8,11,12,13,14;
    row1<<BlockElement(3,4,false,&tmp);
    mat<<row1;
    BlockElement::Zero(2,2).print(0);
    mat.print();

    mat.finish();
    mat.print();
    I2<< 2, 4, 6, 8;
    tmp<< 11,12,13,14,15,16,17,18,111,112,113,114;
    mat.print();
    // mat.update();
    mat.list_rows[0].elements[0].refresh_data(mat.data.data());
    mat.print();
    mat.refresh_data();
    mat.print();


    // mat.populate_data(data.data());

    // mat.populate_starts(starts.data()); 
    // int length[39] = {2,1 2, 2, 2, 2, 1, 1, 2};
    // int columns[372] = {0, 4, 0, 1, 1, 2, 0, 3, 0, 4, 2, 3, 0, 4};
}
}