#include "ProblemMatrices.hpp"
#include <iostream>
#include <math.h> 
namespace exo_lp{

SolutionX::SolutionX()
: status(-1)
, tau_a(8)
, foot_1_wrench(6)
, foot_2_wrench(6)
, lambda_0_p(6)
, lambda_0_n(6)
, lambda_1_p(6)
, lambda_1_n(6)
, lambda_2_p(6)
, lambda_2_n(6)
, contact_1_slack(10)
, contact_2_slack(10)
{
}

Profiler prof1, prof2, prof3, prof4, prof5, prof6, prof7;

SolutionX& SolutionX::operator>>(Solution& sol){
    sol.status = this->status;
    sol.tau_a << this->tau_a;
    sol.foot_1_wrench << this->foot_1_wrench;
    sol.foot_2_wrench << this->foot_2_wrench;
    sol.com_err << this->lambda_0_p - this->lambda_0_n;
    sol.foot_1_err << this->lambda_1_p - this->lambda_1_n;
    sol.foot_2_err << this->lambda_2_p - this->lambda_2_n;
    sol.contact_1_slack << this->contact_1_slack;
    sol.contact_2_slack << this->contact_2_slack;
    return *this;
}

LPMatrices::LPMatrices()
    : S01(6,6)
    , S02(6,6)
    , S10(6,8)
    , S11(6,6)
    , S12(6,6)
    , S20(6,8)
    , S21(6,6)
    , S22(6,6)
    , gamma_1(1,1)
    , gamma_2(1,1)
    , E1(10,6)
    , E2(10,6)
    , tau_max(8)
    , tau_min(8)
    , b0(6)
    , b1(6)
    , b2(6)
    , W0(6)
    , W1(6)
    , W2(6)
    , sensitivity_test(std::pow(10., -7.0))
{
}

bool LPMatrices::check_finite()
{
    bool ret=true;
    Eigen::MatrixXd* mats[12] = {
        &S01, &S02, &S10, &S11, &S12, &S20, &S21, &S22,
        &gamma_1, &gamma_2, &E1, &E2
    };
    Eigen::VectorXd* vecs[8] = {&tau_max,&tau_min,&b0,&b1,&b2,&W0,&W1,&W2};
    for (int i=0;i<12;i++){
        for (int j=0; j<mats[i]->rows();j++){
            for (int k=0; k<mats[i]->cols();k++){
                ret and_eq std::isfinite((*mats[i])(j,k));
        }}}
    for (int i=0;i<8;i++){
        for (int j=0; j<vecs[i]->rows();j++){
            ret and_eq std::isfinite((*vecs[i])(j));
        }}
    return ret;
}

void LPMatrices::print()
{
    std::cout<< "-----------------------------------------"<<std::endl;
    std::cout<< S01 << std::endl;
    std::cout<< S02 << std::endl;
    std::cout<< S10 << std::endl;
    std::cout<< S11 << std::endl;
    std::cout<< S12 << std::endl;
    std::cout<< S20 << std::endl;
    std::cout<< S21 << std::endl;
    std::cout<< S22 << std::endl;
    std::cout<< gamma_1 << std::endl;
    std::cout<< gamma_2 << std::endl;
    std::cout<< E1 << std::endl;
    std::cout<< E2 << std::endl;
    std::cout<< tau_max.transpose() << std::endl;
    std::cout<< tau_min.transpose() << std::endl;
    std::cout<< b0.transpose() << std::endl;
    std::cout<< b1.transpose() << std::endl;
    std::cout<< b2.transpose() << std::endl;
    std::cout<< W0.transpose() << std::endl;
    std::cout<< W1.transpose() << std::endl;
    std::cout<< W2.transpose() << std::endl;
    std::cout<< "-----------------------------------------\n\n"<<std::endl;
}


LPMatrices& LPMatrices::operator<<(LPMatrices& other){
    this->S01 << other.S01;
    this->S02 << other.S02;
    this->S10 << other.S10;
    this->S11 << other.S11;
    this->S12 << other.S12;
    this->S20 << other.S20;
    this->S21 << other.S21;
    this->S22 << other.S22;
    this->gamma_1 << other.gamma_1;
    this->gamma_2 << other.gamma_2;
    this->E1 << other.E1;
    this->E2 << other.E2;
    this->tau_max << other.tau_max;
    this->tau_min << other.tau_min;
    this->b0 << other.b0;
    this->b1 << other.b1;
    this->b2 << other.b2;
    this->W0 << other.W0;
    this->W1 << other.W1;
    this->W2 << other.W2;
    this->sensitivity_test = other.sensitivity_test;
    return *this;
}

LPMatrices& LPMatrices::operator<<(ProblemMatrices& pm){
    this->S01 << pm.S_v.transpose()*pm.J_1.transpose();
    this->S02 << pm.S_v.transpose()*pm.J_2.transpose();

    // b0
    static Eigen::VectorXd SvTB_gmat(6);        //, SvTB_gmat(6) // b0
    static Eigen::VectorXd KpcomSvTJc0TFc0mat(6);    //, KpcomSvTJc0TFc0mat(6) // b0
    static Eigen::VectorXd KpcomSvTJc1TFc1mat(6);    //, KpcomSvTJc1TFc1mat(6) // b0
    static Eigen::VectorXd KpcomSvTJc2TFc2mat(6);    //, KpcomSvTJc2TFc2mat(6) // b0

    SvTB_gmat << pm.S_v.transpose()*pm.B_g_vector;
    KpcomSvTJc0TFc0mat << pm.Kp_com * pm.S_v.transpose() * pm.J_C0.transpose() * pm.f_C0;
    KpcomSvTJc1TFc1mat << pm.Kp_com * pm.S_v.transpose() * pm.J_C1.transpose() * pm.f_C1;
    KpcomSvTJc2TFc2mat << pm.Kp_com * pm.S_v.transpose() * pm.J_C2.transpose() * pm.f_C2;

    this->b0 << SvTB_gmat+KpcomSvTJc0TFc0mat 
        + (1-pm.fuzzy_1_contact)*KpcomSvTJc1TFc1mat
        +(1-pm.fuzzy_2_contact)*KpcomSvTJc2TFc2mat;

    //////////////////
    this->S10 << pm.J_1*pm.A_inverse*pm.S_a;
    this->S11 << pm.J_1*pm.A_inverse*pm.J_1.transpose();
    this->S12 << pm.J_1*pm.A_inverse*pm.J_2.transpose();

    // b1
    static Eigen::VectorXd J1dotqdotmat(6); //    , J1dotqdotmat(6) // b1
    static Eigen::VectorXd J1AinvBgmat(6); //    , J1AinvBgmat(6) // b1
    static Eigen::VectorXd J1AinvJc0Tfc0mat(6); //    , J1AinvJc0Tfc0mat(6) // b1
    static Eigen::VectorXd J1AinvJc2Tfc2mat(6); //    , J1AinvJc2Tfc2mat(6) // b1
    static Eigen::VectorXd Kpswing1J1AinvJc1Tfc1mat(6); //    , Kpswing1J1AinvJc1Tfc1mat(6) // b1
    static Eigen::VectorXd Kpswing1J1AinvJc0Tfc0mat(6); //    , Kpswing1J1AinvJc0Tfc0mat(6) // b1

    J1dotqdotmat << Eigen::VectorXd::Zero(6);
    J1AinvBgmat << pm.J_1*pm.A_inverse*pm.B_g_vector;
    J1AinvJc0Tfc0mat << pm.J_1*pm.A_inverse*pm.J_C0.transpose()*pm.f_C0;
    J1AinvJc2Tfc2mat << pm.J_1*pm.A_inverse*pm.J_C2.transpose()*pm.f_C2;
    Kpswing1J1AinvJc1Tfc1mat << pm.Kp_swing_1*pm.J_1*pm.A_inverse*pm.J_C1.transpose()*pm.f_C1;
    Kpswing1J1AinvJc0Tfc0mat << pm.Kp_swing_1*pm.J_1*pm.A_inverse*pm.J_C0.transpose()*pm.f_C0;

    this->b1 << J1AinvBgmat - J1dotqdotmat
        -pm.fuzzy_1_contact*J1AinvJc0Tfc0mat
        -pm.fuzzy_1_contact*J1AinvJc2Tfc2mat
        +(1-pm.fuzzy_1_contact)*Kpswing1J1AinvJc1Tfc1mat
        +(1-pm.fuzzy_1_contact)*Kpswing1J1AinvJc0Tfc0mat;

    //////////////////
    this->S20 << pm.J_2*pm.A_inverse*pm.S_a;
    this->S21 << pm.J_2*pm.A_inverse*pm.J_1.transpose();
    this->S22 << pm.J_2*pm.A_inverse*pm.J_2.transpose();

    // b2
    static Eigen::VectorXd J2dotqdotmat(6);//    , J2dotqdotmat(6) // b2
    static Eigen::VectorXd J2AinvBgmat(6);//    , J2AinvBgmat(6) // b2
    static Eigen::VectorXd J2AinvJc0Tfc0mat(6);//    , J2AinvJc0Tfc0mat(6) // b2
    static Eigen::VectorXd J2AinvJc1Tfc1mat(6);//    , J2AinvJc1Tfc1mat(6) // b2
    static Eigen::VectorXd Kpswing1J2AinvJc2Tfc2mat(6);//    , Kpswing1J2AinvJc2Tfc2mat(6) // b2
    static Eigen::VectorXd Kpswing1J2AinvJc0Tfc0mat(6);//    , Kpswing1J2AinvJc0Tfc0mat(6) // b2

    J2dotqdotmat << Eigen::VectorXd::Zero(6);
    J2AinvBgmat << pm.J_2*pm.A_inverse*pm.B_g_vector;
    J2AinvJc0Tfc0mat << pm.J_2*pm.A_inverse*pm.J_C0.transpose()*pm.f_C0;
    J2AinvJc1Tfc1mat << pm.J_2*pm.A_inverse*pm.J_C1.transpose()*pm.f_C1;
    Kpswing1J2AinvJc2Tfc2mat << pm.Kp_swing_2*pm.J_2*pm.A_inverse*pm.J_C2.transpose()*pm.f_C2;
    Kpswing1J2AinvJc0Tfc0mat << pm.Kp_swing_2*pm.J_2*pm.A_inverse*pm.J_C0.transpose()*pm.f_C0;

    this->b2 << J2AinvBgmat - J2dotqdotmat
        -pm.fuzzy_2_contact*J2AinvJc0Tfc0mat
        -pm.fuzzy_2_contact*J2AinvJc1Tfc1mat        
        +(1-pm.fuzzy_2_contact)*Kpswing1J2AinvJc2Tfc2mat
        +(1-pm.fuzzy_2_contact)*Kpswing1J2AinvJc0Tfc0mat;

    /////////////////
    this->E1 << Eigen::MatrixXd::Zero(10,6);
    this->E2 << Eigen::MatrixXd::Zero(10,6);
    this->E1(0,3)=1; this->E1(0,5)=-pm.mu; // f_1(3) - mu f_1(5)<=0
    this->E1(1,3)=-1; this->E1(1,5)=-pm.mu; // -f_1(3) - mu f_1(5)<=0
    this->E1(2,4)=1; this->E1(2,5)=-pm.mu; // f_1(4) - mu f_1(5)<=0
    this->E1(3,4)=-1; this->E1(3,5)=-pm.mu; // -f_1(4) - mu f_1(5)<=0
    this->E1(4,2)=1; this->E1(4,5)=-pm.alpha; // f_1(2) - alpha f_1(5)<=0
    this->E1(5,2)=-1; this->E1(5,5)=-pm.alpha; // -f_1(2) - alpha f_1(5)<=0
    this->E1(6,1)=1; this->E1(6,5)=-pm.dy; // f_1(1) - dy f_1(5)<=0
    this->E1(7,1)=-1; this->E1(7,5)=-pm.dy; // -f_1(1) - dy f_1(5)<=0
    this->E1(8,0)=1; this->E1(8,5)=-pm.dx_1_front; // f_1(0) - dx_1_front f_1(5)<=0
    this->E1(9,0)=-1; this->E1(9,5)=-pm.dx_1_back; // -f_1(0) - dx_1_back f_1(5)<=0

    this->E2 << this->E1;
    this->E2(8,5) = -pm.dx_2_front;
    this->E2(9,5) = -pm.dx_2_back;

    this->gamma_1<<pm.gamma_1;
    this->gamma_2<<-pm.gamma_2;

    this->W0<<pm.W_0_data;
    this->W1<<pm.W_1_data;
    this->W2<<pm.W_2_data;


    this->tau_max<<60*Eigen::VectorXd::Ones(8);
    this->tau_min<<-60*Eigen::VectorXd::Ones(8);
    return *this;
}

ProblemMatrices::ProblemMatrices(){
    f_C0=Eigen::Matrix<double, 6, 1>::Zero();
    f_C1=Eigen::Matrix<double, 6, 1>::Zero();
    f_C2=Eigen::Matrix<double, 6, 1>::Zero();

    mass_matrix <<  
             5.31905,   -0.0446141,     0.751247,  4.31946e-16,      7.43171,      1.07346,      2.07911,    -0.355868,    -0.255596,   0.00606448,  -0.00421583,    0.0258896,      2.00689,    0.0640157,    -0.200542,   -0.0184694,    0.0037522,    0.0250026,
          -0.0446141,       5.1347,     0.458197,     -7.43171,  1.36186e-16,      2.35046,     0.158762,      1.97071,   0.00494224,     0.630962,    0.0254031,   0.00429962,    0.0208785,      2.02764,   -0.0339139,     0.632892,    0.0256896,  -0.00381984,
            0.751247,     0.458197,      2.23655,     -1.07346,     -2.35046, -2.33369e-16,     0.513689,     0.856439,    0.0588487,     0.239949,   0.00782036,   0.00249366,     0.484798,    -0.354047,    0.0556556,   -0.0644809, -0.000691476,   0.00234181,
         4.31946e-16,     -7.43171,     -1.07346,      46.0457,  -3.0555e-16, -3.58136e-15,    -0.867791,     -3.79634,    -0.206794,     -1.03547,   -0.0304943,  -0.00559498,      0.35417,     -3.79577,     0.234328,     -1.01143,   -0.0288737,   0.00452821,
             7.43171,  1.36186e-16,     -2.35046,  -3.0555e-16,      46.0457,  1.83284e-16,      3.29957,    -0.303546,    -0.252088,   -0.0532319,   0.00224694,    0.0322538,      3.66337,   -0.0516513,    -0.190063,   -0.0217699,   0.00348305,    0.0313227,
             1.07346,      2.35046, -2.33369e-16, -3.58136e-15,  1.83284e-16,      46.0457,      1.50305,    -0.804844,    -0.117279,     0.169449,   -0.0233584,   0.00249235,    -0.613439,    -0.816761,    0.0543716,     0.087489,   -0.0246701,   0.00272875,

             2.07911,    0.158762,    0.513689,   -0.867791,      3.29957,      1.50305,      1.81961,   0.00242147,     -0.18754,    0.0580836,  0.000729019,    0.0225744,            0,            0,            0,            0,            0,            0,
           -0.355868,     1.97071,    0.856439,    -3.79634,    -0.303546,    -0.804844,   0.00242147,      1.99237,    0.0391912,     0.633796,    0.0251575,   0.00134693,            0,            0,            0,            0,            0,            0,
           -0.255596,  0.00494224,   0.0588487,   -0.206794,    -0.252088,    -0.117279,     -0.18754,    0.0391912,     0.098134,   -0.0179781,    -0.001008,  -0.00589957,            0,            0,            0,            0,            0,            0,
          0.00606448,    0.630962,    0.239949,    -1.03547,   -0.0532319,     0.169449,    0.0580836,     0.633796,   -0.0179781,     0.265944,    0.0114777,   0.00174803,            0,            0,            0,            0,            0,            0,
         -0.00421583,   0.0254031,  0.00782036,  -0.0304943,   0.00224694,   -0.0233584,  0.000729019,    0.0251575,    -0.001008,    0.0114777,   0.00370283,  0.000381004,            0,            0,            0,            0,            0,            0,
           0.0258896,  0.00429962,  0.00249366, -0.00559498,    0.0322538,   0.00249235,    0.0225744,   0.00134693,  -0.00589957,   0.00174803,  0.000381004,   0.00227055,            0,            0,            0,            0,            0,            0,
        
             2.00689,   0.0208785,    0.484798,     0.35417,      3.66337,    -0.613439,            0,            0,            0,            0,            0,            0,      1.84131,   0.00138472,    -0.149579,    0.0117618,   0.00213041,    0.0222653,
           0.0640157,     2.02764,   -0.354047,    -3.79577,   -0.0516513,    -0.816761,            0,            0,            0,            0,            0,            0,   0.00138472,      1.97405,   -0.0250293,     0.624791,    0.0250106,  -0.00421321,
           -0.200542,  -0.0339139,   0.0556556,    0.234328,    -0.190063,    0.0543716,            0,            0,            0,            0,            0,            0,    -0.149579,   -0.0250293,    0.0846193,    0.0144913,   0.00078302,  -0.00496848,
          -0.0184694,    0.632892,  -0.0644809,    -1.01143,   -0.0217699,     0.087489,            0,            0,            0,            0,            0,            0,    0.0117618,     0.624791,    0.0144913,      0.25604,    0.0117896,  -0.00207032,
           0.0037522,   0.0256896,-0.000691476,  -0.0288737,   0.00348305,   -0.0246701,            0,            0,            0,            0,            0,            0,   0.00213041,    0.0250106,   0.00078302,    0.0117896,   0.00377285, -0.000229597,
           0.0250026, -0.00381984,  0.00234181,  0.00452821,    0.0313227,   0.00272875,            0,            0,            0,            0,            0,            0,    0.0222653,  -0.00421321,  -0.00496848,  -0.00207032, -0.000229597,   0.00219224
    ;
    Eigen::LLT<MassMatrixType> A_LLT(mass_matrix);
    A_inverse=A_LLT.solve(MassMatrixType::Identity());
    assert((A_inverse *(mass_matrix)-MassMatrixType::Identity()).norm()<1e-7);
    B_g_vector << 
         3.45248,
         30.1445,
         2.59587,
        -7.68023,
        -37.8762, // tilted y
         450.052, // tilted z

         11.6112,
        -4.82949,
        -0.93236,
         3.19813,
       -0.206587,
    -6.88311e-06,
        -9.11862,
        -3.37898,
        0.483001,
         2.52812,
       -0.220979,
     -0.00411361;

    J_1 <<
            0.98739,    0.140571,  -0.0728129,           0,           0,           0,    0.818697,    0.187264,  -0.0637388,    0.172776,    0.138733,    0.999982,           0,           0,           0,           0,           0,           0,
          -0.146634,    0.985448,  -0.0859714,           0,           0,           0,   -0.169975,    0.981997,  -0.0130454,    0.984671,    0.987483,           0,           0,           0,           0,           0,           0,           0,
          0.0596682,   0.0955642,    0.993633,           0,           0,           0,    0.548492,   0.0247991,    0.997881,   0.0239086,  -0.0750324, -0.00594752,           0,           0,           0,           0,           0,           0,
           0.115626,   -0.826811,  -0.0282585,     0.98739,    0.140571,  -0.0728129,    0.140521,   -0.814709,   0.0101783,   -0.437215,   -0.108664,           0,           0,           0,           0,           0,           0,           0,
           0.813681,    0.108303,   -0.146405,   -0.146634,    0.985448,  -0.0859714,    0.613693,    0.152431,   -0.170745,   0.0733217,   0.0147656,        0.11,           0,           0,           0,           0,           0,           0,
           0.086235,   0.0993965,  -0.0147381,   0.0596682,   0.0955642,    0.993633,  -0.0195666,     0.11611, -0.00158204,    0.139792, -0.00659041,           0,           0,           0,           0,           0,           0,           0
    ;
    J_2 <<
         0.986447,   -0.157605,   -0.045638,           0,           0,           0,           0,           0,           0,           0,           0,           0,    0.831469, 7.72146e-06,   -0.139227,   -0.179272,    -0.13946,    0.999991,
         0.152023,    0.982548,   -0.107189,           0,           0,           0,           0,           0,           0,           0,           0,           0,   0.0780612,    0.990078,     0.13915,    0.970177,    0.987891,           0,
         0.061735,   0.0987979,    0.993191,           0,           0,           0,           0,           0,           0,           0,           0,           0,     0.55006,   -0.140517,    0.980435,   -0.163152,  -0.0679815, -0.00414382,
        -0.114011,   -0.795879,    0.284174,    0.986447,   -0.157605,   -0.045638,           0,           0,           0,           0,           0,           0, -0.00839432,   -0.816188,  -0.0149724,   -0.426084,   -0.108696,           0,
         0.813863,   -0.138255,   -0.113036,    0.152023,    0.982548,   -0.107189,           0,           0,           0,           0,           0,           0,    0.613165,   0.0152724,    -0.21932,  -0.0508468,  -0.0157851,        0.11,
          -0.1824,    0.105341,  0.00085883,    0.061735,   0.0987979,    0.993191,           0,           0,           0,           0,           0,           0,  -0.0743278,    0.107564,   0.0290012,    0.165824, -0.00640319,           0;


    S_v_T = Eigen::Matrix<double, 6, 18, Eigen::RowMajor>::Zero();
    S_v_T.block<6,6>(0,0) = Eigen::Matrix<double, 6, 6, Eigen::RowMajor>::Identity();
    S_v=Eigen::Matrix<double, 18, 6, Eigen::RowMajor> (S_v_T.transpose());

    J_C2=Eigen::Matrix<double, 6, 18, Eigen::RowMajor> (J_2); //TODO: get this properly from the cuff frame
    J_C1=Eigen::Matrix<double, 6, 18, Eigen::RowMajor> (J_1); //TODO: get this properly from the cuff frame
    J_C0=Eigen::Matrix<double, 6, 18, Eigen::RowMajor> (S_v_T); //TODO: get this properly from the cuff frame
    Kp_com=Eigen::Matrix<double, 6, 6, Eigen::RowMajor>::Identity();
    Kp_swing_1=Eigen::Matrix<double, 6, 6, Eigen::RowMajor>::Identity();
    Kp_swing_2=Eigen::Matrix<double, 6, 6, Eigen::RowMajor>::Identity();


    S_a = Eigen::Matrix<double, 18, 8, Eigen::RowMajor>::Zero();
    S_a(Joints::l_AA,0)=1;
    S_a(Joints::l_FE,1)=1;
    S_a(Joints::l_KF,2)=1;
    S_a(Joints::l_AF,3)=1;

    S_a(Joints::r_AA,4)=1;
    S_a(Joints::r_FE,5)=1;
    S_a(Joints::r_KF,6)=1;
    S_a(Joints::r_AF,7)=1;


    mu=.25; // coefficient of friction
    alpha=.05; // rotary coefficient of friction
    dy = .025; // half foot width
    dx_1_front = .1; // front? foot length
    dx_1_back = .05; // back? foot length
    z_bar_1 = 5000; // non-limiting (now anyway) maximum z force;


    dx_2_front = .1; // front? foot length
    dx_2_back = .05; // back? foot length
    z_bar_2 = 5000; // non-limiting (now anyway) maximum z force;
    fuzzy_1_contact = 1.0; // sexy-flat-ramps in and out with contact transition
    fuzzy_2_contact = 1.0; // same

    W_0_data << 32, 512, 256, 128, 64, 1028;
    W_1_data << 16, 16, 8, 8, 4, 4;
    W_2_data << 16, 16, 8, 8, 4, 4;

    gamma_1=1.0;
    gamma_2=1.0;
}







// struct LPMatrices{ // doesn't work. Need MatriXd references to build block mat.
//     Mat66d S01;
//     Mat66d S02;
//     Mat68d S10;
//     Mat66d S11;
//     Mat66d S12;
//     Mat68d S20;
//     Mat66d S21;
//     Mat66d S22;
//     Mat11d gamma_1;
//     Mat11d gamma_2;
//     Mat106d E1;
//     Mat106d E2;
//     Vec8d tau_max;
//     Vec8d tau_min;
//     Vec6d b0;
//     Vec6d b1;
//     Vec6d b2;
//     Vec6d W0;
//     Vec6d W1;
//     Vec6d W2;

//     // LPMatrices();
//     LPMatrices& operator<<(LPMatrices& other);
//     LPMatrices& operator<<(ProblemMatrices& pm);
// };
}