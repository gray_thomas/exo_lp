#ifndef EXO_FLOP_CPP_SOLVER_HPP
#define EXO_FLOP_CPP_SOLVER_HPP
#include "SolverInterface.hpp"
#include "ProblemMatrices.hpp"
#include <flopc.hpp>
using namespace flopc;
using namespace std;
#include "OsiClpSolverInterface.hpp"
#include <Eigen/Dense>
#include <Eigen/LU>

using namespace Eigen;

namespace exo_lp{
namespace flop{

struct PreFlopSolver{
    Eigen::Matrix <double, 6, 6, Eigen::RowMajor> SvTJ1Tdata;
    Eigen::Matrix <double, 6, 6, Eigen::RowMajor> SvTJ2Tdata;
    Eigen::Matrix <double, 6, 1> SvTB_gdata;
    Eigen::Matrix <double, 6, 1> F_C0;
    Eigen::Matrix <double, 6, 1> F_C1;
    Eigen::Matrix <double, 6, 1> F_C2;
    Eigen::Matrix <double, 6, 1> KpcomSvTJc0TFc0data;
    Eigen::Matrix <double, 6, 1> KpcomSvTJc1TFc1data;
    Eigen::Matrix <double, 6, 1> KpcomSvTJc2TFc2data;
    Eigen::Matrix <double, 6, 6, Eigen::RowMajor> Beta_COMdata;
    Eigen::Matrix <double, 6, 6, Eigen::RowMajor> Beta_foot_1data;
    Eigen::Matrix <double, 6, 6, Eigen::RowMajor> Beta_foot_2data;

    Eigen::Matrix <double, 6, 8, Eigen::RowMajor> J1AinvSadata;
    Eigen::Matrix <double, 6, 6, Eigen::RowMajor> J1AinvJ1Tdata;
    Eigen::Matrix <double, 6, 6, Eigen::RowMajor> J1AinvJ2Tdata;
    Eigen::Matrix <double, 6, 1> J1dotqdotdata;
    Eigen::Matrix <double, 6, 1> J1AinvBgdata;
    Eigen::Matrix <double, 6, 1> J1AinvJc0Tfc0data;
    Eigen::Matrix <double, 6, 1> J1AinvJc2Tfc2data;
    Eigen::Matrix <double, 6, 1> Kpswing1J1AinvJc1Tfc1data;
    Eigen::Matrix <double, 6, 1> Kpswing1J1AinvJc0Tfc0data;

    Eigen::Matrix <double, 6, 8, Eigen::RowMajor> J2AinvSadata;
    Eigen::Matrix <double, 6, 6, Eigen::RowMajor> J2AinvJ1Tdata;
    Eigen::Matrix <double, 6, 6, Eigen::RowMajor> J2AinvJ2Tdata;
    Eigen::Matrix <double, 6, 1> J2dotqdotdata;
    Eigen::Matrix <double, 6, 1> J2AinvBgdata;
    Eigen::Matrix <double, 6, 1> J2AinvJc0Tfc0data;
    Eigen::Matrix <double, 6, 1> J2AinvJc1Tfc1data;
    Eigen::Matrix <double, 6, 1> Kpswing1J2AinvJc2Tfc2data;
    Eigen::Matrix <double, 6, 1> Kpswing1J2AinvJc0Tfc0data;
    Eigen::Matrix <double, 12, 6, Eigen::RowMajor> foot_constraint_matrix_1_data;
    Eigen::Matrix <double, 12, 6, Eigen::RowMajor> foot_constraint_matrix_2_data;
    Eigen::Matrix <double, 12, 1> foot_constraint_vector_1_data;
    Eigen::Matrix <double, 12, 1> foot_constraint_vector_2_data;

    Eigen::Matrix<double, 2, 1> gamma_data;

    double fuzzy_1_contact, fuzzy_2_contact;

    Eigen::Matrix<double, 43, 56> big_matrix;
    Eigen::Matrix<int, 9, 1> bcs;
    Eigen::Matrix<int, 6, 1> brs;

    Eigen::Matrix<int, 10, 1> bc;
    Eigen::Matrix<int, 7, 1> br;

    PreFlopSolver(ProblemMatrices& pm);
    void print_big_matrix();
};

class FlopCppSolver: public SolverInterface{
    PreFlopSolver pfs;
    MP_set i_6;
    MP_set j_6;
    MP_set j_18;
    MP_set j_8;
    MP_set i_12;
    MP_set i_2;

    MP_variable
        tau_a, // not necessarily positive
        f_1, // not necessarily positive
        f_2, // not necessarily positive
        lambda_p_0, // pos
        lambda_p_1, // pos
        lambda_p_2, // pos
        lambda_n_0, // pos
        lambda_n_1, // pos
        lambda_n_2 // pos
    ;

    MP_constraint 
        com_balance,
        foot_1_balance,
        foot_2_balance,
        foot_1_limit,
        foot_2_limit,
        relative_foot_weight_balance; 

    MP_data SvTJ1T;
    MP_data SvTJ2T;
    MP_data SvTB_g;

    MP_data KpcomSvTJc0TFc0;
    MP_data KpcomSvTJc1TFc1;
    MP_data KpcomSvTJc2TFc2;

    MP_data Beta_COM;
    MP_data Beta_foot_1;
    MP_data Beta_foot_2;
    MP_data J1AinvSa;
    MP_data J1AinvJ1T;
    MP_data J1AinvJ2T;
    MP_data J1dotqdot;
    MP_data J1AinvBg;
    MP_data J1AinvJc0Tfc0;
    MP_data J1AinvJc2Tfc2;
    MP_data Kpswing1J1AinvJc1Tfc1;
    MP_data Kpswing1J1AinvJc0Tfc0;

    // Foot 2 balance coefficients
    MP_data J2AinvSa;
    MP_data J2AinvJ1T;
    MP_data J2AinvJ2T;
    MP_data J2dotqdot;
    MP_data J2AinvBg;
    MP_data J2AinvJc0Tfc0;
    MP_data J2AinvJc1Tfc1;
    MP_data Kpswing2J2AinvJc2Tfc2;
    MP_data Kpswing2J2AinvJc0Tfc0;

    MP_data 
        foot_constraint_matrix_1,
        foot_constraint_matrix_2,
        foot_constraint_vector_1,
        foot_constraint_vector_2;
    MP_data gamma;
    MP_data W_0;
    MP_data W_1;
    MP_data W_2;

    // OsiCbcSolverInterface solver; // 0.00148985 seconds
    OsiClpSolverInterface solver; //  0.00149244 seconds
    // OsiGlpkSolverInterface solver; //  ?? seconds
    // MP_model mymodel(new OsiClpSolverInterface);

    MP_model mymodel;
public:
    FlopCppSolver(ProblemMatrices& pm);
    virtual void setup();
    virtual void solve(ProblemMatrices &pm, Solution &sol);
    virtual void print();
};
}
}
#endif // EXO_FLOP_CPP_SOLVER_HPP