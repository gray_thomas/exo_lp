#ifndef BLOCK_MATRIX_UTILS_HPP
#define BLOCK_MATRIX_UTILS_HPP

#include <Eigen/Dense>
#include <iostream>
using namespace Eigen;
namespace block_mat{

typedef int CoinBigIndex;



struct BlockElement{
    int rows;
    int cols;
    bool is_zero;
    bool is_sparse;
    Eigen::MatrixXd* data;
    Eigen::MatrixXi indexes;
    Eigen::MatrixXi mask;
    int col0;
    int row0;
    int num_elements;

    // convenience constructor
    BlockElement(Eigen::MatrixXd* data);

    // dense/zero constructor
    BlockElement(int rows, int cols, bool is_zero, Eigen::MatrixXd* data);

    //sparse (mask) constructor
    BlockElement(int rows, int cols, Eigen::MatrixXd* data, Eigen::MatrixXi mask);

    void refresh_data(double * datalist);
    void refresh_col_indexes(int * columnslist);
    inline static BlockElement Zero(int rows, int cols){
        return BlockElement(rows, cols, true, NULL);
    }

    void print(int row);
};



struct BlockRow{
    int rows;
    int bcols;
    int row0;
    int cols;
    std::vector<int> col_starts;
    std::vector<BlockElement> elements;
    BlockRow& operator<<(BlockElement el);
    BlockRow();
    void print();
    int num_elements();
};

struct BlockMatrix{
    std::vector<BlockRow> list_rows;
    std::vector<int> row_starts;
    int cols;
    int rows;
    int brows;
    int numberColumns;// = mat.cols; std::cout<<"num columns "<<mat.cols<<std::endl;
    int numberRows;// = mat.rows; std::cout<<"num rows "<<mat.rows<<std::endl;
    int numberElements;// = mat.num_elements(); std::cout<<"num elements "<<mat.num_elements()<<std::endl;
    // matrix data - column ordered
    // CoinBigIndex starts[numberRows+1];
    Eigen::Matrix<CoinBigIndex, 1, Dynamic> starts;//=Eigen::Matrix<CoinBigIndex, 1, Dynamic> ::Zero(numberRows+1);
    Eigen::Matrix<int, 1, Dynamic> length;//=Eigen::Matrix<int, 1, Dynamic>::Zero(numberRows);
    Eigen::Matrix<int, 1, Dynamic> columns;//=Eigen::Matrix<int, 1, Dynamic>::Zero(numberElements);
    Eigen::Matrix<double, 1, Dynamic> data;//=Eigen::Matrix<double, 1, Dynamic>::Zero(numberElements);


    BlockMatrix();
    int num_elements();

    void finish();
    void refresh_data();
    void setup_structure(double* data, int* columns, int* length, CoinBigIndex* starts);
    void print();
    BlockMatrix& operator<<(BlockRow& row);
};

// struct BlockVectorElement{
//     Eigen::VectorXd* data;
//     int size;
//     BlockVectorElement(Eigen::VectorXd* data);
// };

struct BlockVector{
    std::vector<Eigen::VectorXd*> list_elements;
    std::vector<int> starts;
    int size;
    BlockVector();
    BlockVector& operator<<(Eigen::VectorXd* element);
    void copy_to(Eigen::VectorXd& target);
    void copy_from(Eigen::VectorXd& target);
    void print();
};

// GRAYTODO: forbid structural mutability after check.
struct BlockProblem{
    int rows;
    int cols;

    BlockMatrix matrix;

    BlockVector objective;
    BlockVector colLower;
    BlockVector colUpper;
    BlockVector rowLower;
    BlockVector rowUpper;

    BlockVector colPrimalSolution;
    BlockVector rowPrimalSolution;
    BlockVector colDualSolution;
    BlockVector rowDualSolution;

    std::vector<std::string> *rownames; // Null if too lazy
    std::vector<std::string> *columnnames; // only copied if new.

    BlockProblem();

    void print();

    bool check();
};


// typedef Eigen::Matrix<double, Joints::num_joints, Joints::num_joints, Eigen::RowMajor> MassMatrixType;

void test_blocks();
}

#endif//BLOCK_MATRIX_UTILS_HPP