#ifndef LPV4_HPP
#define LPV4_HPP

#include "BlockMatrixUtils.hpp"
#include "GenericSolverInterface.hpp"
#include "GenericProblem.hpp"

#include <Eigen/Dense>

using namespace Eigen;
namespace exo_lp{

class LPV4: public exo_lp::GenericProblem{
    std::vector<std::string> rownames;
    std::vector<std::string> colnames;

    void setup_block_problem();
    void setup_defaults();
    void annotate_model();
public:
	ColumnElement tau_a;
	ColumnElement tau_p;
	ColumnElement monopod;

	PuntingRowElement com;
	MatrixXd com_by_monopod;

	RowElement monopod_motion;
	MatrixXd monopod_motion_by_tau_a;
	MatrixXd monopod_motion_by_tau_p;
	// MatrixXd monopod_motion_by_monopod; // I

	PuntingRowElement foot_spread;
	MatrixXd foot_spread_by_tau_a;
	MatrixXd foot_spread_by_tau_p;
	MatrixXd foot_spread_by_monopod_test; // needed?


	PuntingRowElement monopod_lim;
	MatrixXd monopod_lim_by_monopod;

	PuntingRowElement tau_a_reg;
	PuntingRowElement tau_p_reg;

	int status;

    LPV4();

    void solve(int type=0);
    void print();

};

}


#endif