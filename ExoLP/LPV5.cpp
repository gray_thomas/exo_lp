#include "LPV5.hpp"
#include <limits>

static const double DBL_MIN = (std::numeric_limits< double >::min)();
static const double DBL_MAX = (std::numeric_limits< double >::max)();
static const double NaN = std::numeric_limits<double>::quiet_NaN();

namespace exo_lp
{

LPV5::LPV5()
: tau_a(8)
, tau_p_pos(4)
, tau_p_neg(4)
, hip(6)
, hip_by_tau_a(6,8)
, hip_by_tau_p(6,4)
, diff(6)
, diff_by_tau_a(6,8)
, diff_by_tau_p(6,4)
, f1_lim(10)
, f1_lim_by_tau_a(10,8)
, f1_lim_by_tau_p(10,4)
, f2_lim(10)
, f2_lim_by_tau_a(10,8)
, f2_lim_by_tau_p(10,4)
, status(-1)
{
    annotate_model();
    setup_block_problem();
    gsi = newGenericSolver(this->bp);
}


void LPV5::annotate_model()
{

}

void LPV5::solve(int type)
{
    status = this->gsi->solve(type);
}

void LPV5::print()
{
    bp.print();
}

void LPV5::setup_defaults()
{
    // columns limits
    tau_a.lower.setConstant(-60); // updates
    tau_a.upper.setConstant(60); // updates

    tau_p_pos.lower.setZero();
    tau_p_pos.upper.setConstant(20);
    tau_p_neg.lower.setConstant(-20);
    tau_p_neg.upper.setZero();

    hip.punt_pos.lower.setZero();
    hip.punt_pos.upper.setConstant(DBL_MAX);
    hip.punt_neg.lower.setZero();
    hip.punt_neg.upper.setConstant(DBL_MAX);


    diff.punt_pos.lower.setZero();
    diff.punt_pos.upper.setConstant(DBL_MAX);
    diff.punt_neg.lower.setZero();
    diff.punt_neg.upper.setConstant(DBL_MAX);


    f1_lim.punt_pos.lower.setZero();
    f1_lim.punt_pos.upper.setConstant(DBL_MAX);
    f1_lim.punt_neg.lower.setConstant(NaN);// punt_neg unused for inequality.
    f1_lim.punt_neg.upper.setConstant(NaN);


    f2_lim.punt_pos.lower.setZero();
    f2_lim.punt_pos.upper.setConstant(DBL_MAX);
    f2_lim.punt_neg.lower.setConstant(NaN);// punt_neg unused for inequality.
    f2_lim.punt_neg.upper.setConstant(NaN);


    // row limits
    hip.lower.setZero(); 
    hip.upper.setZero(); 
    diff.lower.setZero(); 
    diff.upper.setZero(); 
    f1_lim.lower.setConstant(-DBL_MAX);
    f1_lim.upper.setZero();
    f2_lim.lower.setConstant(-DBL_MAX);
    f2_lim.upper.setZero();

    // obj
    tau_a.obj.setZero();
    tau_p_pos.obj.setOnes();
    tau_p_neg.obj.setConstant(-1.);
    hip.punt_cost.setOnes();
    diff.punt_cost.setConstant(100);
    f1_lim.punt_cost.setOnes();
    f2_lim.punt_cost.setOnes();

    // these are dense now.
    f1_lim_by_tau_a << Eigen::MatrixXd::Zero(10,8);
    f2_lim_by_tau_a << Eigen::MatrixXd::Zero(10,8);
   
}

void LPV5::setup_block_problem(){
    setup_defaults();

    Eigen::MatrixXd *NEye6 = nmc(-1*Eigen::MatrixXd::Identity(6,6));
    Eigen::MatrixXd *Eye6 = nmc(Eigen::MatrixXd::Identity(6,6));
    Eigen::MatrixXi Eye6mask = Eigen::MatrixXi::Identity(6,6);


    this->bp.matrix << ((*nbr()) // hip task
        << BE(&hip_by_tau_a) // tau_a
        << BE(&hip_by_tau_p) // tau_p_pos
        << BE(&hip_by_tau_p) // tau_p_neg
        << BE(6,6,Eye6,Eye6mask)<< BE(6,6,NEye6,Eye6mask) // hip punt
        << BE::Zero(6,12) // diff punt
        << BE::Zero(6,10) // f1_lim punt
        << BE::Zero(6,10) // f2_lim punt
        );

    this->bp.matrix << ((*nbr()) // diff task
        << BE(&diff_by_tau_a) // tau_a
        << BE(&diff_by_tau_p) // tau_p_pos
        << BE(&diff_by_tau_p) // tau_p_neg
        << BE::Zero(6,12) // hip punt
        << BE(6,6,Eye6,Eye6mask)<< BE(6,6,NEye6,Eye6mask) // diff punt
        << BE::Zero(6,10) // f1_lim punt
        << BE::Zero(6,10) // f2_lim punt
        );

    this->bp.matrix << ((*nbr()) // f1_lim (inequality) task
        << BE(&f1_lim_by_tau_a) // tau_a
        << BE(&f1_lim_by_tau_p) // tau_p_pos
        << BE(&f1_lim_by_tau_p) // tau_p_neg
        << BE::Zero(10,12) // hip punt
        << BE::Zero(10,12) // diff punt
        << BE(10, 10, nmc(-Eigen::MatrixXd::Identity(10,10)), Eigen::MatrixXi::Identity(10,10)) // f1_lim punt
        << BE::Zero(10,10) // f2_lim punt
        );

    this->bp.matrix << ((*nbr()) // f2_lim (inequality) task
        << BE(&f2_lim_by_tau_a) // tau_a
        << BE(&f2_lim_by_tau_p) // tau_p_pos
        << BE(&f2_lim_by_tau_p) // tau_p_neg
        << BE::Zero(10,12) // hip punt
        << BE::Zero(10,12) // diff punt
        << BE::Zero(10,10) // f1_lim punt
        << BE(10, 10, nmc(-Eigen::MatrixXd::Identity(10,10)), Eigen::MatrixXi::Identity(10,10)) // f2_lim punt
        );


    this->bp.matrix.finish();

    RowElement* _rows[]={&hip, &diff, &f1_lim, &f2_lim};
    RowElement* _cols[]={&tau_a, &tau_p_pos, &tau_p_neg, &hip.punt_pos, &hip.punt_neg,
        &diff.punt_pos, &diff.punt_neg, 
        &f1_lim.punt_pos,
        &f2_lim.punt_pos
    };


    std::vector<RowElement*> rows(_rows, _rows + sizeof(_rows)/sizeof(_rows[0]));
    std::vector<RowElement*> cols(_cols, _cols + sizeof(_cols)/sizeof(_cols[0]));


    this->bp.objective
        << &tau_a.obj
        << &tau_p_pos.obj
        << &tau_p_neg.obj
        << &hip.punt_cost
        << &hip.punt_cost
        << &diff.punt_cost
        << &diff.punt_cost
        << &f1_lim.punt_cost
        << &f2_lim.punt_cost
        ;

    for (int c=0;c<int(cols.size());c++){
        this->bp.colLower << &cols[c]->lower;
        this->bp.colUpper << &cols[c]->upper;
        this->bp.colPrimalSolution << &cols[c]->dat;
        this->bp.colDualSolution << &cols[c]->dual;
    }

    for (int r=0;r<int(rows.size());r++){
        this->bp.rowLower << &rows[r]->lower;
        this->bp.rowUpper << &rows[r]->upper;
        this->bp.rowPrimalSolution << &rows[r]->dat;
        this->bp.rowDualSolution << &rows[r]->dual;
    }
    this->bp.check();
}


}