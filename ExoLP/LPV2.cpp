#include "LPV2.hpp"
#include <limits>

static const double DBL_MIN = (std::numeric_limits< double >::min)();
static const double DBL_MAX = (std::numeric_limits< double >::max)();
static const double NaN = std::numeric_limits<double>::quiet_NaN();

namespace exo_lp
{

LPV2::LPV2()
: tau_a(8)
, foot1(6)
, foot2(6)

, com(6)
, com_by_foot1(6, 6)
, com_by_foot2(6, 6)

, foot_center(6)
, foot_center_by_tau_a(6, 8)
, foot_center_by_foot1(6, 6)
, foot_center_by_foot2(6, 6)

, foot_spread(6)
, foot_spread_by_tau_a(6, 8)

, foot_equality(1)
, foot_equality_gamma1(1, 1)
, foot_equality_gamma2(1, 1)

, foot1_lim(10)
, foot1_lim_by_foot1(10, 6)

, foot2_lim(10)
, foot2_lim_by_foot2(10, 6)
{
    annotate_model();
    setup_block_problem();
    gsi = newGenericSolver(this->bp);
}


void LPV2::annotate_model()
{

}

void LPV2::solve()
{
    // bp.print();
    status = this->gsi->solve();
}

void LPV2::setup_defaults()
{
    // columns limits
    tau_a.lower.setConstant(-60); // updates
    tau_a.upper.setConstant(60); // updates
    foot1.lower << -DBL_MAX*VectorXd::Ones(5), 0;
    foot1.upper.setConstant(DBL_MAX);
    foot2.lower << -DBL_MAX*VectorXd::Ones(5), 0;
    foot2.upper.setConstant(DBL_MAX);
    com.punt_pos.lower.setZero();
    com.punt_pos.upper.setConstant(DBL_MAX);
    com.punt_neg.lower.setZero();
    com.punt_neg.upper.setConstant(DBL_MAX);
    foot_center.punt_pos.lower.setZero();
    foot_center.punt_pos.upper.setConstant(DBL_MAX);
    foot_center.punt_neg.lower.setZero();
    foot_center.punt_neg.upper.setConstant(DBL_MAX);
    foot_spread.punt_pos.lower.setZero();
    foot_spread.punt_pos.upper.setConstant(DBL_MAX);
    foot_spread.punt_neg.lower.setZero();
    foot_spread.punt_neg.upper.setConstant(DBL_MAX);

    // row limits
    com.lower.setZero(); // updates
    com.upper.setZero(); // updates
    foot_center.lower.setZero(); // updates
    foot_center.upper.setZero(); // updates
    foot_spread.lower.setZero(); // updates
    foot_spread.upper.setZero(); // updates
    foot_equality.lower.setZero();
    foot_equality.upper.setZero();
    foot1_lim.lower.setConstant(-DBL_MAX);
    foot1_lim.upper.setZero();
    foot2_lim.lower.setConstant(-DBL_MAX);
    foot2_lim.upper.setZero();

    // obj
    tau_a.obj.setZero();
    foot1.obj.setZero();
    foot2.obj.setZero();
    com.punt_cost.setOnes();
    foot_center.punt_cost.setOnes();
    foot_spread.punt_cost.setOnes();

    // com -- no constants
    // foot_center -- no constants
    // foot_spread -- no constants
    // foot_equality
    foot_equality_gamma1.setConstant(.5);
    foot_equality_gamma2.setConstant(-.5);

    // foot1_lim
    foot1_lim_by_foot1 << Eigen::MatrixXd::Zero(10,6);
    foot1_lim_by_foot1(0,3)=1; foot1_lim_by_foot1(0,5)=-.25; // f_1(3) - mu f_1(5)<=0
    foot1_lim_by_foot1(1,3)=-1; foot1_lim_by_foot1(1,5)=-.25; // -f_1(3) - mu f_1(5)<=0
    foot1_lim_by_foot1(2,4)=1; foot1_lim_by_foot1(2,5)=-.25; // f_1(4) - mu f_1(5)<=0
    foot1_lim_by_foot1(3,4)=-1; foot1_lim_by_foot1(3,5)=-.25; // -f_1(4) - mu f_1(5)<=0
    foot1_lim_by_foot1(4,2)=1; foot1_lim_by_foot1(4,5)=-.05; // f_1(2) - alpha f_1(5)<=0
    foot1_lim_by_foot1(5,2)=-1; foot1_lim_by_foot1(5,5)=-.05; // -f_1(2) - alpha f_1(5)<=0
    foot1_lim_by_foot1(6,1)=1; foot1_lim_by_foot1(6,5)=-.025; // f_1(1) - dy f_1(5)<=0
    foot1_lim_by_foot1(7,1)=-1; foot1_lim_by_foot1(7,5)=-.025; // -f_1(1) - dy f_1(5)<=0
    foot1_lim_by_foot1(8,0)=1; foot1_lim_by_foot1(8,5)=-.1; // f_1(0) - dx_1_front f_1(5)<=0
    foot1_lim_by_foot1(9,0)=-1; foot1_lim_by_foot1(9,5)=-.05; // -f_1(0) - dx_1_back f_1(5)<=0

    // foot2_lim
    foot2_lim_by_foot2 << foot1_lim_by_foot1;

}

void LPV2::setup_block_problem(){
    setup_defaults();

    Eigen::MatrixXd *NEye6 = nmc(-1*Eigen::MatrixXd::Identity(6,6));
    Eigen::MatrixXd *Eye6 = nmc(Eigen::MatrixXd::Identity(6,6));
    Eigen::MatrixXi Eye6mask = Eigen::MatrixXi::Identity(6,6);

    Eigen::MatrixXi foot_lim_mask(10,6);
    foot_lim_mask.setZero();
    foot_lim_mask.block<10,1>(0,5).setOnes();
    foot_lim_mask.block<2,1>(0,3).setOnes();
    foot_lim_mask.block<2,1>(2,4).setOnes();
    foot_lim_mask.block<2,1>(4,2).setOnes();
    foot_lim_mask.block<2,1>(6,1).setOnes();
    foot_lim_mask.block<2,1>(8,0).setOnes();

    Eigen::MatrixXi com_foot_mask=Eigen::MatrixXi::Ones(6,6);
    com_foot_mask.block<3,3>(3,0)<<Eigen::MatrixXi::Zero(3,3);

    this->bp.matrix << ((*nbr()) << BE::Zero(6,8) 
        << BE(6,6,&com_by_foot1, com_foot_mask)<<BE(6,6,&com_by_foot2, com_foot_mask)
        << BE(6,6,Eye6,Eye6mask)<< BE(6,6,NEye6,Eye6mask)<<BE::Zero(6,24));

    this->bp.matrix << ((*nbr()) << BE(&foot_center_by_tau_a) << BE(&foot_center_by_foot1) << BE(&foot_center_by_foot2)
        <<BE::Zero(6,12) << BE(6,6,Eye6,Eye6mask)<< BE(6,6,NEye6,Eye6mask)<<BE::Zero(6,12));

    this->bp.matrix << ((*nbr()) << BE(&foot_spread_by_tau_a) << BE::Zero(6,36)
        << BE(6,6,Eye6,Eye6mask)<< BE(6,6,NEye6,Eye6mask));

    this->bp.matrix << ((*nbr()) << BE::Zero(1,8)
        << BE::Zero(1,5) << BE(&foot_equality_gamma1)
        << BE::Zero(1,5) << BE(&foot_equality_gamma2) << BE::Zero(1,36));

    this->bp.matrix << ((*nbr())<<BE::Zero(10,8)<<BE(10,6,&foot1_lim_by_foot1, foot_lim_mask)<<BE::Zero(10,42));

    this->bp.matrix << ((*nbr())<<BE::Zero(10,8+6)<<BE(10,6,&foot2_lim_by_foot2, foot_lim_mask)<<BE::Zero(10,36));

    this->bp.matrix.finish();

    this->bp.objective << &tau_a.obj << & foot1.obj << &foot2.obj
        << &com.punt_cost << &com.punt_cost
        << &foot_center.punt_cost << &foot_center.punt_cost
        << &foot_spread.punt_cost << &foot_spread.punt_cost;
    
    this->bp.colLower << &tau_a.lower << &foot1.lower << &foot2.lower 
        << &com.punt_pos.lower << &com.punt_neg.lower 
        << &foot_center.punt_pos.lower << &foot_center.punt_neg.lower
        << &foot_spread.punt_pos.lower << &foot_spread.punt_neg.lower;

    this->bp.colUpper << &tau_a.upper << &foot1.upper << &foot2.upper 
        << &com.punt_pos.upper << &com.punt_neg.upper 
        << &foot_center.punt_pos.upper << &foot_center.punt_neg.upper
        << &foot_spread.punt_pos.upper << &foot_spread.punt_neg.upper;

    this->bp.rowLower << &com.lower << &foot_center.lower<< &foot_spread.lower << &foot_equality.lower 
        << &foot1_lim.lower << &foot2_lim.lower;

    this->bp.rowUpper << &com.upper << &foot_center.upper<< &foot_spread.upper << &foot_equality.upper 
        << &foot1_lim.upper << &foot2_lim.upper;

    this->bp.colPrimalSolution << &tau_a.dat << &foot1.dat << &foot2.dat 
        << &com.punt_pos.dat << &com.punt_neg.dat 
        << &foot_center.punt_pos.dat << &foot_center.punt_neg.dat
        << &foot_spread.punt_pos.dat << &foot_spread.punt_neg.dat;

    this->bp.colDualSolution << &tau_a.dual << &foot1.dual << &foot2.dual 
        << &com.punt_pos.dual << &com.punt_neg.dual 
        << &foot_center.punt_pos.dual << &foot_center.punt_neg.dual
        << &foot_spread.punt_pos.dual << &foot_spread.punt_neg.dual;

    this->bp.rowPrimalSolution << &com.dat << &foot_center.dat<< &foot_spread.dat << &foot_equality.dat 
        << &foot1_lim.dat << &foot2_lim.dat;

    this->bp.rowDualSolution << &com.dual << &foot_center.dual<< &foot_spread.dual << &foot_equality.dual 
        << &foot1_lim.dual << &foot2_lim.dual;

    this->bp.check();
}


}