#ifndef LPV5_HPP
#define LPV5_HPP

#include "BlockMatrixUtils.hpp"
#include "GenericSolverInterface.hpp"
#include "GenericProblem.hpp"

#include <Eigen/Dense>

using namespace Eigen;
namespace exo_lp{

class LPV5: public exo_lp::GenericProblem{
    std::vector<std::string> rownames;
    std::vector<std::string> colnames;

    void setup_block_problem();
    void setup_defaults();
    void annotate_model();
public:
	ColumnElement tau_a;
	ColumnElement tau_p_pos;
	ColumnElement tau_p_neg;

	PuntingRowElement hip;
	MatrixXd hip_by_tau_a;
	MatrixXd hip_by_tau_p;

	PuntingRowElement diff;
	MatrixXd diff_by_tau_a;
	MatrixXd diff_by_tau_p;

	PuntingRowElement f1_lim;
	MatrixXd f1_lim_by_tau_a;
	MatrixXd f1_lim_by_tau_p;

	PuntingRowElement f2_lim;
	MatrixXd f2_lim_by_tau_a;
	MatrixXd f2_lim_by_tau_p;


	int status;

    LPV5();

    void solve(int type=0);
    void print();

};

}


#endif