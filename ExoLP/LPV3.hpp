#ifndef LPV3_HPP
#define LPV3_HPP

#include "BlockMatrixUtils.hpp"
#include "GenericSolverInterface.hpp"
#include "GenericProblem.hpp"

#include <Eigen/Dense>

using namespace Eigen;
namespace exo_lp{

class LPV3: public exo_lp::GenericProblem{
    std::vector<std::string> rownames;
    std::vector<std::string> colnames;

    void setup_block_problem();
    void setup_defaults();
    void annotate_model();
public:
	ColumnElement tau_a;
	ColumnElement monopod;

	PuntingRowElement com;
	MatrixXd com_by_monopod;

	RowElement monopod_motion;
	MatrixXd monopod_motion_by_tau_a;
	MatrixXd monopod_motion_by_monopod;

	PuntingRowElement foot_spread;
	MatrixXd foot_spread_by_tau_a;
	MatrixXd foot_spread_by_monopod_test; // needed?


	PuntingRowElement monopod_lim;
	MatrixXd monopod_lim_by_monopod;

	int status;

    LPV3();

    void solve();

};

}


#endif