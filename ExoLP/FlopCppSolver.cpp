#include "FlopCppSolver.hpp"

namespace exo_lp{
namespace flop{

PreFlopSolver::PreFlopSolver(ProblemMatrices& pm){
    F_C0 = Eigen::Matrix< double, 6, 1>::Zero();
    F_C1 = Eigen::Matrix< double, 6, 1>::Zero();
    F_C2 = Eigen::Matrix< double, 6, 1>::Zero();
    KpcomSvTJc0TFc0data = pm.Kp_com * pm.S_v.transpose() * pm.J_C0.transpose() * F_C0;
    KpcomSvTJc1TFc1data = pm.Kp_com * pm.S_v.transpose() * pm.J_C1.transpose() * F_C1;
    KpcomSvTJc2TFc2data = pm.Kp_com * pm.S_v.transpose() * pm.J_C2.transpose() * F_C2;
    Beta_COMdata=Eigen::Matrix<double, 6, 6, Eigen::RowMajor>::Identity();
    Beta_foot_1data=Eigen::Matrix<double, 6, 6, Eigen::RowMajor>::Identity();
    Beta_foot_2data=Eigen::Matrix<double, 6, 6, Eigen::RowMajor>::Identity();

    SvTJ1Tdata = pm.S_v.transpose()*pm.J_1.transpose();
    SvTJ2Tdata = pm.S_v.transpose()*pm.J_2.transpose();
    SvTB_gdata = pm.S_v.transpose()*pm.B_g_vector;
    
    J1AinvSadata = pm.J_1*pm.A_inverse*pm.S_a;
    J1AinvJ1Tdata = pm.J_1*pm.A_inverse*pm.J_1.transpose();
    J1AinvJ2Tdata = pm.J_1*pm.A_inverse*pm.J_2.transpose();
    J1dotqdotdata = Eigen::Matrix < double, 6, 1 >::Zero();
    J1AinvBgdata = pm.J_1*pm.A_inverse*pm.B_g_vector;
    J1AinvJc0Tfc0data = pm.J_1*pm.A_inverse*pm.J_C0.transpose()*pm.f_C0;
    J1AinvJc2Tfc2data = pm.J_1*pm.A_inverse*pm.J_C2.transpose()*pm.f_C2;
    Kpswing1J1AinvJc1Tfc1data = pm.Kp_swing_1*pm.J_1*pm.A_inverse*pm.J_C1.transpose()*pm.f_C1;
    Kpswing1J1AinvJc0Tfc0data = pm.Kp_swing_1*pm.J_1*pm.A_inverse*pm.J_C0.transpose()*pm.f_C0;
    
    J2AinvSadata = pm.J_2*pm.A_inverse*pm.S_a;
    J2AinvJ1Tdata = pm.J_2*pm.A_inverse*pm.J_1.transpose();
    J2AinvJ2Tdata = pm.J_2*pm.A_inverse*pm.J_2.transpose();
    J2dotqdotdata = Eigen::Matrix < double, 6, 1 >::Zero();
    J2AinvBgdata = pm.J_2*pm.A_inverse*pm.B_g_vector;
    J2AinvJc0Tfc0data = pm.J_2*pm.A_inverse*pm.J_C0.transpose()*pm.f_C0;
    J2AinvJc1Tfc1data = pm.J_2*pm.A_inverse*pm.J_C1.transpose()*pm.f_C1;
    Kpswing1J2AinvJc2Tfc2data = pm.Kp_swing_2*pm.J_2*pm.A_inverse*pm.J_C2.transpose()*pm.f_C2;
    Kpswing1J2AinvJc0Tfc0data = pm.Kp_swing_2*pm.J_2*pm.A_inverse*pm.J_C0.transpose()*pm.f_C0;


    foot_constraint_matrix_1_data = Eigen::Matrix< double, 12, 6, Eigen::RowMajor>::Zero();
    foot_constraint_matrix_2_data = Eigen::Matrix< double, 12, 6, Eigen::RowMajor>::Zero();
    foot_constraint_vector_1_data = Eigen::Matrix< double, 12, 1>::Zero();
    foot_constraint_vector_2_data = Eigen::Matrix< double, 12, 1>::Zero();
    foot_constraint_matrix_1_data(0,5)=-1.0; // -f_1(5)<=0
    foot_constraint_matrix_1_data(1,5)=1.0; // f_1(5)<= z_bar_1
    foot_constraint_matrix_1_data(2,3)=1; foot_constraint_matrix_1_data(2,5)=-pm.mu; // f_1(3) - pm.mu f_1(5)<=0
    foot_constraint_matrix_1_data(3,3)=-1; foot_constraint_matrix_1_data(3,5)=-pm.mu; // -f_1(3) - pm.mu f_1(5)<=0
    foot_constraint_matrix_1_data(4,4)=1; foot_constraint_matrix_1_data(4,5)=-pm.mu; // f_1(4) - pm.mu f_1(5)<=0
    foot_constraint_matrix_1_data(5,4)=-1; foot_constraint_matrix_1_data(5,5)=-pm.mu; // -f_1(4) - mu f_1(5)<=0
    foot_constraint_matrix_1_data(6,2)=1; foot_constraint_matrix_1_data(6,5)=-pm.alpha; // f_1(2) - pm.alpha f_1(5)<=0
    foot_constraint_matrix_1_data(7,2)=-1; foot_constraint_matrix_1_data(7,5)=-pm.alpha; // -f_1(2) - pm.alpha f_1(5)<=0
    foot_constraint_matrix_1_data(8,1)=1; foot_constraint_matrix_1_data(8,5)=-pm.dy; // f_1(1) - pm.dy f_1(5)<=0
    foot_constraint_matrix_1_data(9,1)=-1; foot_constraint_matrix_1_data(9,5)=-pm.dy; // -f_1(1) - dy f_1(5)<=0
    foot_constraint_matrix_1_data(10,0)=1; foot_constraint_matrix_1_data(10,5)=-pm.dx_1_front; // f_1(0) - dx_1_front f_1(5)<=0
    foot_constraint_matrix_1_data(11,0)=-1; foot_constraint_matrix_1_data(11,5)=-pm.dx_1_back; // -f_1(0) - dx_1_back f_1(5)<=0


    foot_constraint_matrix_2_data<<foot_constraint_matrix_1_data;
    foot_constraint_matrix_2_data(10,5)=-pm.dx_2_front; // f_1(0) - dx_1_front f_1(5)<=0
    foot_constraint_matrix_2_data(11,5)=-pm.dx_2_back; // -f_1(0) - dx_1_back f_1(5)<=0

    foot_constraint_vector_1_data(1)=pm.z_bar_1;
    foot_constraint_vector_2_data(1)=pm.z_bar_2;

    gamma_data<< 1, 1;



    fuzzy_1_contact=pm.fuzzy_1_contact;
    fuzzy_2_contact=pm.fuzzy_2_contact;

}


void PreFlopSolver::print_big_matrix(){
    big_matrix=Eigen::Matrix<double, 43, 56>::Zero();
    bcs << 8, 6, 6, 6, 6, 6, 6, 6, 6; // block column sizes
    brs << 6, 6, 6, 12, 12, 1; // block row sizes

    bc << 0,8,14,20,26,32,38,44,50,56; // based on bcs
    br << 0,6,12,18,30,42,43; // based on brs

    big_matrix.block<6,6>(br(0),bc(1)) << SvTJ1Tdata;
    big_matrix.block<6,6>(br(0),bc(2)) << SvTJ2Tdata;
    big_matrix.block<6,6>(br(0),bc(3)) << Beta_COMdata;
    big_matrix.block<6,6>(br(0),bc(4)) << -Beta_COMdata;

    big_matrix.block<6,8>(br(1),bc(0)) << J1AinvSadata;
    big_matrix.block<6,6>(br(1),bc(1)) << J1AinvJ1Tdata;
    big_matrix.block<6,6>(br(1),bc(2)) << J1AinvJ2Tdata;
    big_matrix.block<6,6>(br(1),bc(5)) << Beta_foot_1data;
    big_matrix.block<6,6>(br(1),bc(6)) << -Beta_foot_1data;

    big_matrix.block<6,8>(br(2),bc(0)) << J2AinvSadata;
    big_matrix.block<6,6>(br(2),bc(1)) << J2AinvJ1Tdata;
    big_matrix.block<6,6>(br(2),bc(2)) << J2AinvJ2Tdata;
    big_matrix.block<6,6>(br(2),bc(7)) << Beta_foot_2data;
    big_matrix.block<6,6>(br(2),bc(8)) << -Beta_foot_2data;

    big_matrix.block<12,6>(br(3),bc(1)) << foot_constraint_matrix_1_data;

    big_matrix.block<12,6>(br(4),bc(2)) << foot_constraint_matrix_2_data;

    big_matrix(br(5),bc(1)+5) = gamma_data(0);
    big_matrix(br(5),bc(2)+5) = -gamma_data(1);

    std::cout<<big_matrix<<std::endl;
}


FlopCppSolver::FlopCppSolver(ProblemMatrices& pm):
    pfs(pm),
    i_6(6),
    j_6(6),
    j_18(18),
    j_8(8),
    i_12(12),
    i_2(2),
    tau_a(j_8), // not necessarily positive
    f_1(j_6), // not necessarily positive
    f_2(j_6), // not necessarily positive
    lambda_p_0(j_6), // pos
    lambda_p_1(j_6), // pos
    lambda_p_2(j_6), // pos
    lambda_n_0(j_6), // pos
    lambda_n_1(j_6), // pos
    lambda_n_2(j_6), // pos
    com_balance(i_6),
    foot_1_balance(i_6),
    foot_2_balance(i_6),
    foot_1_limit(i_12),
    foot_2_limit(i_12),
    relative_foot_weight_balance(),
    SvTJ1T(pfs.SvTJ1Tdata.data(),i_6,j_6),
    SvTJ2T(pfs.SvTJ2Tdata.data(),i_6,j_6),
    SvTB_g (pfs.SvTB_gdata.data(),i_6),
    KpcomSvTJc0TFc0(pfs.KpcomSvTJc0TFc0data.data(),i_6),
    KpcomSvTJc1TFc1(pfs.KpcomSvTJc1TFc1data.data(),i_6),
    KpcomSvTJc2TFc2(pfs.KpcomSvTJc2TFc2data.data(),i_6),
    Beta_COM(pfs.Beta_COMdata.data(), i_6, j_6),
    Beta_foot_1(pfs.Beta_foot_1data.data(), i_6, j_6),
    Beta_foot_2(pfs.Beta_foot_2data.data(), i_6, j_6),
    J1AinvSa(pfs.J1AinvSadata.data(),i_6, j_8),
    J1AinvJ1T(pfs.J1AinvJ1Tdata.data(),i_6, j_6),
    J1AinvJ2T(pfs.J1AinvJ2Tdata.data(),i_6, j_6),
    J1dotqdot(pfs.J1dotqdotdata.data(),i_6),
    J1AinvBg(pfs.J1AinvBgdata.data(),i_6),
    J1AinvJc0Tfc0(pfs.J1AinvJc0Tfc0data.data(),i_6),
    J1AinvJc2Tfc2(pfs.J1AinvJc2Tfc2data.data(),i_6),
    Kpswing1J1AinvJc1Tfc1(pfs.Kpswing1J1AinvJc1Tfc1data.data(),i_6),
    Kpswing1J1AinvJc0Tfc0(pfs.Kpswing1J1AinvJc0Tfc0data.data(),i_6),
    J2AinvSa(pfs.J2AinvSadata.data(),i_6, j_8),
    J2AinvJ1T(pfs.J2AinvJ1Tdata.data(),i_6, j_6),
    J2AinvJ2T(pfs.J2AinvJ2Tdata.data(),i_6, j_6),
    J2dotqdot(pfs.J2dotqdotdata.data(),i_6),
    J2AinvBg(pfs.J2AinvBgdata.data(),i_6),
    J2AinvJc0Tfc0(pfs.J2AinvJc0Tfc0data.data(),i_6),
    J2AinvJc1Tfc1(pfs.J2AinvJc1Tfc1data.data(),i_6),
    Kpswing2J2AinvJc2Tfc2(pfs.Kpswing1J2AinvJc2Tfc2data.data(),i_6),
    Kpswing2J2AinvJc0Tfc0(pfs.Kpswing1J2AinvJc0Tfc0data.data(),i_6),
    gamma(pfs.gamma_data.data(),i_2),
    W_0(pm.W_0_data.data(),i_6),
    W_1(pm.W_1_data.data(),i_6),
    W_2(pm.W_2_data.data(),i_6),
    foot_constraint_matrix_1(pfs.foot_constraint_matrix_1_data.data(), i_12, j_6),
    foot_constraint_matrix_2(pfs.foot_constraint_matrix_2_data.data(), i_12, j_6),
    foot_constraint_vector_1(pfs.foot_constraint_vector_1_data.data(), i_12),
    foot_constraint_vector_2(pfs.foot_constraint_vector_2_data.data(), i_12),
    solver(),
    mymodel(&solver)
{
    //Default lower limit for all variables is zero (why??)
    tau_a.lowerLimit(j_8) = -50.0; // don't know how to specify it per element
    tau_a.upperLimit(j_8) = 50.0; // don't know how to specify it per element
    f_1.lowerLimit(j_6) = -10000; // don't know how to specify it per element
    f_2.lowerLimit(j_6) = -10000; // don't know how to specify it per element
    f_1.upperLimit(j_6) = 10000; // don't know how to specify it per element
    f_2.upperLimit(j_6) = 10000; // don't know how to specify it per element

    com_balance(i_6) = (
            sum(j_6.such_that(SvTJ1T(i_6,j_6)!=0), SvTJ1T(i_6,j_6)*f_1(j_6)) // (fades in with 1-contact)
            // sum(j_6, pm.fuzzy_1_contact*SvTJ1T(i_6,j_6)*f_1(j_6)) // (fades in with 1-contact)
            +sum(j_6.such_that(SvTJ2T(i_6,j_6)!=0), SvTJ2T(i_6,j_6)*f_2(j_6)) // (fades in with 2-contact)
            // +sum(j_6, pm.fuzzy_2_contact*SvTJ2T(i_6,j_6)*f_2(j_6)) // (fades in with 2-contact)
        ) == (
            SvTB_g(i_6) // always exists
            +KpcomSvTJc0TFc0(i_6) // under implemented (always exists)
            +(1-pfs.fuzzy_1_contact)*KpcomSvTJc1TFc1(i_6) // under implemented (fades out in 1-contact)
            +(1-pfs.fuzzy_2_contact)*KpcomSvTJc2TFc2(i_6) // under implemented (fades out in 2-contact)
            + sum(j_6.such_that(Beta_COM(i_6, j_6)!=0), Beta_COM(i_6, j_6)*lambda_p_0(j_6)) 
            - sum(j_6.such_that(Beta_COM(i_6, j_6)!=0), Beta_COM(i_6, j_6)*lambda_n_0(j_6))
        );//INCOMPLETE

    foot_1_balance(i_6) = (
            sum(j_8.such_that(J1AinvSa(i_6, j_8)!=0), J1AinvSa(i_6, j_8)*tau_a(j_8)) // always
            // sum(j_8, J1AinvSa(i_6, j_8)*tau_a(j_8)) // always
            +sum(j_6.such_that(J1AinvJ1T(i_6, j_6)!=0), J1AinvJ1T(i_6, j_6)*f_1(j_6)) // fades in with 1-contact
            // +sum(j_6, pfs.fuzzy_1_contact*J1AinvJ1T(i_6, j_6)*f_1(j_6)) // fades in with 1-contact
            +sum(j_6.such_that(J1AinvJ2T(i_6, j_6)!=0), J1AinvJ2T(i_6, j_6)*f_2(j_6)) // fades in with 2-contact
            // +sum(j_6, pfs.fuzzy_2_contact*J1AinvJ2T(i_6, j_6)*f_2(j_6)) // fades in with 2-contact
        ) == (
            J1AinvBg(i_6) // always
            -1*J1dotqdot(i_6) // always
            -pfs.fuzzy_1_contact*J1AinvJc0Tfc0(i_6) // fades in with 1-contact
            -pfs.fuzzy_1_contact*J1AinvJc2Tfc2(i_6) // fades in with 1-contact
            +(1-pfs.fuzzy_1_contact)*Kpswing1J1AinvJc1Tfc1(i_6) // fades out in 1-contact
            +(1-pfs.fuzzy_1_contact)*Kpswing1J1AinvJc0Tfc0(i_6) // fades out in 1-contact
            +sum(j_6.such_that(Beta_foot_1(i_6, j_6)!=0), Beta_foot_1(i_6, j_6)*lambda_p_1(j_6)) 
            -sum(j_6.such_that(Beta_foot_1(i_6, j_6)!=0), Beta_foot_1(i_6, j_6)*lambda_n_1(j_6)) 
            // +sum(j_6, Beta_foot_1(i_6, j_6)*lambda_1(j_6)) // under implemented (shifts top priority frame definition according to human COP)
        );

    foot_2_balance(i_6) = (
            sum(j_8.such_that(J2AinvSa(i_6, j_8)!=0), J2AinvSa(i_6, j_8)*tau_a(j_8)) // always
            // sum(j_8, J2AinvSa(i_6, j_8)*tau_a(j_8)) // always
            +sum(j_6.such_that(J2AinvJ1T(i_6, j_6)!=0), J2AinvJ1T(i_6, j_6)*f_1(j_6)) // fades in with 1-contact
            // +sum(j_6, pfs.fuzzy_1_contact*J2AinvJ1T(i_6, j_6)*f_1(j_6)) // fades in with 1-contact
            +sum(j_6.such_that(J2AinvJ2T(i_6, j_6)!=0), J2AinvJ2T(i_6, j_6)*f_2(j_6)) // fades in with 2-contact
            // +sum(j_6, pfs.fuzzy_2_contact*J2AinvJ2T(i_6, j_6)*f_2(j_6)) // fades in with 2-contact
        ) == (
            J2AinvBg(i_6) // always
            -1*J2dotqdot(i_6) // always
            -pfs.fuzzy_2_contact*J2AinvJc0Tfc0(i_6) // fades in with 2-contact
            -pfs.fuzzy_2_contact*J2AinvJc1Tfc1(i_6) // fades in with 2-contact
            +(1-pfs.fuzzy_2_contact)*Kpswing2J2AinvJc2Tfc2(i_6) // fades out in 2-contact
            +(1-pfs.fuzzy_2_contact)*Kpswing2J2AinvJc0Tfc0(i_6) // fades out in 2-contact
            +sum(j_6.such_that(Beta_foot_2(i_6, j_6)!=0), Beta_foot_2(i_6, j_6)*lambda_p_2(j_6))
            -sum(j_6.such_that(Beta_foot_2(i_6, j_6)!=0), Beta_foot_2(i_6, j_6)*lambda_n_2(j_6))  // fades out in 2-contact
            // +sum(j_6, Beta_foot_2(i_6, j_6)*lambda_2(j_6)) // fades out in 2-contact
        );


    foot_1_limit(i_12) = (
            // sum(j_6, foot_constraint_matrix_1(i_12, j_6)*f_1(j_6))//699
            sum(j_6.such_that(foot_constraint_matrix_1(i_12, j_6) != 0), foot_constraint_matrix_1(i_12, j_6)*f_1(j_6))//649
        ) <= (
            foot_constraint_vector_1(i_12)
        );

    foot_2_limit(i_12) = (
            // sum(j_6, foot_constraint_matrix_2(i_12, j_6)*f_2(j_6))
            sum(j_6.such_that(foot_constraint_matrix_2(i_12, j_6) != 0), foot_constraint_matrix_2(i_12, j_6)*f_2(j_6))
        ) <= (
            foot_constraint_vector_2(i_12)
        );

    relative_foot_weight_balance() = gamma(0)*f_1(5) == gamma(1)*f_2(5);


    mymodel
        .add(com_balance)
        .add(foot_1_balance)
        .add(foot_2_balance)
        .add(relative_foot_weight_balance)// this also makes it solve faster
        // .add(cost_def)
        .add(foot_1_limit)
        .add(foot_2_limit)
        ;

}
void FlopCppSolver::setup(){

    mymodel.silent();
    // mymodel.verbose();
    auto cost =  // TODO cost is local
        sum(i_6, 
            W_0(i_6) * lambda_p_0(i_6)
            + W_0(i_6) * lambda_n_0(i_6)
            + W_1(i_6) * lambda_p_1(i_6)
            + W_1(i_6) * lambda_n_1(i_6)
            + W_2(i_6) * lambda_p_2(i_6)
            + W_2(i_6) * lambda_n_2(i_6)
            ) ;
    mymodel.minimize(cost);
    // cout<<"model status "<<mymodel.getStatus()<<std::endl; 
}
void FlopCppSolver::solve(ProblemMatrices &pm, Solution &sol){
    pfs.gamma_data<< pm.gamma_1, pm.gamma_2;
    relative_foot_weight_balance() = gamma(0)*f_1(5) == gamma(1)*f_2(5);
    auto cost =  // TODO cost is local
        sum(i_6, 
            W_0(i_6) * lambda_p_0(i_6)
            + W_0(i_6) * lambda_n_0(i_6)
            + W_1(i_6) * lambda_p_1(i_6)
            + W_1(i_6) * lambda_n_1(i_6)
            + W_2(i_6) * lambda_p_2(i_6)
            + W_2(i_6) * lambda_n_2(i_6)
            ) ;
    mymodel.minimize(cost);
    sol.status = mymodel.getStatus();
    for (int i=0;i<8;i++) sol.tau_a(i)=tau_a.level(i);
    for (int i=0;i<6;i++) sol.foot_1_wrench(i)=f_1.level(i);
    for (int i=0;i<6;i++) sol.foot_2_wrench(i)=f_2.level(i);
    for (int i=0;i<6;i++) sol.com_err(i)=lambda_p_0.level(i)-lambda_n_0.level(i);
    for (int i=0;i<6;i++) sol.foot_1_err(i)=lambda_p_1.level(i)-lambda_n_1.level(i);
    for (int i=0;i<6;i++) sol.foot_2_err(i)=lambda_p_2.level(i)-lambda_n_2.level(i);
    for (int i=0;i<10;i++) sol.contact_1_slack(i)=sol.contact_1_slack(i)+0.1;//TODO
    for (int i=0;i<10;i++) sol.contact_1_slack(i)=W_0(i%6);//TODO

}
void FlopCppSolver::print(){
    std::cout<<"tau_a: "; for (int i=0;i<8;i++) {std::cout<<tau_a.level(i)<<", ";} std::cout<<"\n";
    std::cout<<"f_1: "; for (int i=0;i<6;i++) {std::cout<<f_1.level(i)<<", ";} std::cout<<"\n";
    std::cout<<"f_2: "; for (int i=0;i<6;i++) {std::cout<<f_2.level(i)<<", ";} std::cout<<"\n";
    std::cout<<"p_0: "; for (int i=0;i<6;i++) {std::cout<<lambda_p_0.level(i)-lambda_n_0.level(i)<<", ";} std::cout<<"\n";
    std::cout<<"p_1: "; for (int i=0;i<6;i++) {std::cout<<lambda_p_1.level(i)-lambda_n_1.level(i)<<", ";} std::cout<<"\n";
    std::cout<<"p_2: "; for (int i=0;i<6;i++) {std::cout<<lambda_p_2.level(i)-lambda_n_2.level(i)<<", ";} std::cout<<"\n";
}


}
SolverInterface* newFlopCppSolver(ProblemMatrices &pm){
    return new flop::FlopCppSolver(pm);
}
}