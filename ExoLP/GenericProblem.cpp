#include "GenericProblem.hpp"
#include <limits>

static const double DBL_MIN = (std::numeric_limits< double >::min)();
static const double DBL_MAX = (std::numeric_limits< double >::max)();
static const double NaN = std::numeric_limits<double>::quiet_NaN();

namespace exo_lp
{


GenericProblem::GenericProblem()
    : gsi(NULL)

{
}

Eigen::VectorXd* GenericProblem::nvc(const Eigen::VectorXd& vec)
{
    this->const_vectors.emplace_back(new Eigen::VectorXd(vec));
    return this->const_vectors.back();
}

Eigen::MatrixXd* GenericProblem::nmc(const Eigen::MatrixXd& mat)
{
    this->const_mats.emplace_back(new Eigen::MatrixXd(mat));
    return this->const_mats.back();
}
block_mat::BlockRow* GenericProblem::nbr()
{
    this->block_rows.emplace_back(new block_mat::BlockRow());
    return this->block_rows.back();
}
GenericProblem::~GenericProblem()
{
    for (int i=0;i<this->const_vectors.size();i++){
        delete this->const_vectors[i];
    }
    for (int i=0;i<this->const_mats.size();i++){
        delete this->const_mats[i];
    }    
    for (int i=0;i<this->block_rows.size();i++){
        delete this->block_rows[i];
    }
    delete gsi;
}

RowElement::RowElement(int size)
: dat(size)
, upper(size)
, lower(size)
, dual(size)
{
    dat.setConstant(NaN);
    upper.setConstant(NaN);
    lower.setConstant(NaN);
    dual.setConstant(NaN);
}

ColumnElement::ColumnElement(int size)
: RowElement(size)
, obj(size)
{
    obj.setConstant(NaN);
}

PuntColumn::PuntColumn(int size)
: RowElement(size)
{

}

PuntingRowElement::PuntingRowElement(int size)
: RowElement(size)
, punt_pos(size)
, punt_neg(size)
, punt_cost(size)
{
    punt_cost.setConstant(NaN);
}

}