#ifndef LOW_LEVEL_SOLVER_HPP
#define LOW_LEVEL_SOLVER_HPP
#include "SolverInterface.hpp"
#include "BlockMatrixUtils.hpp"
#include "ClpSimplex.hpp"
#include "CoinHelperFunctions.hpp"
#include "ProblemMatrices.hpp"

namespace exo_lp{
namespace lowlevel{
using namespace block_mat;
class LowLevelSolver: public SolverInterface{
    // result storage
    Eigen::VectorXd colPrimalSolution;
    Eigen::VectorXd rowPrimalSolution;
    Eigen::VectorXd colDualSolution;
    Eigen::VectorXd rowDualSolution;

    LPMatrices m_lpm;
    Solution m_sol;

    Eigen::VectorXd objective;
    Eigen::VectorXd colLower;
    Eigen::VectorXd colUpper;
    Eigen::VectorXd rowLower;
    Eigen::VectorXd rowUpper;


    // sparsity masks
    Eigen::MatrixXi wrench_limit_mask_mat;
    Eigen::MatrixXi COM_jac_mask;

    // constant data
    Eigen::MatrixXd Eye6;
    Eigen::MatrixXd NEye6;
    Eigen::MatrixXi Eye6mask;

    // Block Matrix elements
    BlockMatrix blockmodel;
    BlockRow centroid_wrench_balance;
    BlockRow foot_1_accel_balance;
    BlockRow foot_2_accel_balance;
    BlockRow foot_equality;
    BlockRow foot1_inequality;
    BlockRow foot2_inequality;

    // could this be local?
    CoinPackedMatrix matrix;

    // load problem (could this be local too?)
    ClpSimplex model;

    // used only for print.
    // Eigen::VectorXd tau_a_res;
    // Eigen::VectorXd f_r_1_res;
    // Eigen::VectorXd f_r_2_res;
    // Eigen::VectorXd err_0_res;
    // Eigen::VectorXd err_1_res;
    // Eigen::VectorXd err_2_res;

    void annotate_model();
    void load_problem();
    void setup_block_matrix();
    void update_problem_data();
    void update_problem_data(ProblemMatrices& pm);
    void solve(Solution &sol);
public:
    LowLevelSolver(ProblemMatrices& pm);
    virtual void setup();
    virtual void solve(LPMatrices &lpm, Solution &sol);
    virtual void solve(ProblemMatrices &pm, Solution &sol); // deprecated
    virtual void print();
};


}
}
#endif//LOW_LEVEL_SOLVER_HPP
