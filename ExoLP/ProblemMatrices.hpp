#ifndef PROBLEM_MATRICES_HPP
#define PROBLEM_MATRICES_HPP
#include <Eigen/Dense>
#include <chrono>

namespace exo_lp{
namespace Joints {
    enum{v_roll, v_pitch, v_yaw, v_x, v_y, v_z, //0, 1, 2, 3, 4, 5
        l_AA, l_FE, l_PS, l_KF, l_AF, l_AR, // id 6, 7, 8, 9, 10, 11
        r_AA, r_FE, r_PS, r_KF, r_AF, r_AR, // id 12, 13, 14, 15, 16, 17
        num_joints//18
    };
}

typedef Eigen::Matrix<double, Joints::num_joints, Joints::num_joints, Eigen::RowMajor> MassMatrixType;
typedef Eigen::Matrix<double, 6, 8> Mat68d;
typedef Eigen::Matrix<double, 6, 6> Mat66d;
typedef Eigen::Matrix<double, 6, 1> Vec6d;
typedef Eigen::Matrix<double, 8, 1> Vec8d;
typedef Eigen::Matrix<double, 10,6> Mat106d;
typedef Eigen::Matrix<double, 1, 1> Mat11d;
typedef Eigen::Matrix<double, 10, 1> Vec10d;
typedef Eigen::Matrix<double, 18, 1> Vec18d;
typedef Eigen::Matrix<double, 6, 18> Mat618d;
typedef Eigen::Matrix<double, 18, 6> Mat186d;
typedef Eigen::Matrix<double, 18, 8> Mat188d;

struct ProblemMatrices{
    Eigen::Matrix<double, Joints::num_joints, Joints::num_joints, Eigen::RowMajor> mass_matrix;
    MassMatrixType A_inverse;
    Vec18d B_g_vector;
    Mat618d J_1;
    Mat618d J_2;
    Mat618d S_v_T;
    Mat186d S_v;
    Mat618d J_C2;
    Mat618d J_C1;
    Mat618d J_C0;
    Mat66d Kp_com;
    Mat66d Kp_swing_1;
    Mat66d Kp_swing_2;
    Mat188d S_a;
        // Spoof cuff sensors with fake data
    Vec6d f_C0;
    Vec6d f_C1;
    Vec6d f_C2;

    double mu, alpha, dy, dx_1_front, dx_1_back, z_bar_1;
    double dx_2_front, dx_2_back, z_bar_2;
    double fuzzy_1_contact;
    double fuzzy_2_contact;

    double gamma_1;
    double gamma_2;

    Vec6d W_0_data;
    Vec6d W_1_data;
    Vec6d W_2_data;

    ProblemMatrices();
};

struct LPMatrices{
    Eigen::MatrixXd S01;
    Eigen::MatrixXd S02;
    Eigen::MatrixXd S10;
    Eigen::MatrixXd S11;
    Eigen::MatrixXd S12;
    Eigen::MatrixXd S20;
    Eigen::MatrixXd S21;
    Eigen::MatrixXd S22;
    Eigen::MatrixXd gamma_1;
    Eigen::MatrixXd gamma_2;
    Eigen::MatrixXd E1;
    Eigen::MatrixXd E2;
    Eigen::VectorXd tau_max;
    Eigen::VectorXd tau_min;
    Eigen::VectorXd b0;
    Eigen::VectorXd b1;
    Eigen::VectorXd b2;
    Eigen::VectorXd W0;
    Eigen::VectorXd W1;
    Eigen::VectorXd W2;

    double sensitivity_test;

    LPMatrices();
    bool check_finite();
    void print();
    LPMatrices& operator<<(LPMatrices& other);
    LPMatrices& operator<<(ProblemMatrices& pm);
};

struct Solution{
    int status;
    Vec8d tau_a;
    Vec6d foot_1_wrench;
    Vec6d foot_2_wrench;
    Vec6d com_err;
    Vec6d foot_1_err;
    Vec6d foot_2_err;
    Vec10d contact_1_slack;
    Vec10d contact_2_slack;
};

struct SolutionX{
    int status;
    Eigen::VectorXd tau_a;
    Eigen::VectorXd foot_1_wrench;
    Eigen::VectorXd foot_2_wrench;
    Eigen::VectorXd lambda_0_p;
    Eigen::VectorXd lambda_0_n;
    Eigen::VectorXd lambda_1_p;
    Eigen::VectorXd lambda_1_n;
    Eigen::VectorXd lambda_2_p;
    Eigen::VectorXd lambda_2_n;
    Eigen::VectorXd contact_1_slack;
    Eigen::VectorXd contact_2_slack;
    SolutionX();
    
    SolutionX& operator >>(Solution& sol);
};



struct Profiler{
    std::chrono::microseconds total;
    int N;
    bool started;
    std::chrono::steady_clock::time_point start_time;

    Profiler() : total(0), N(0), started(false){

    }

    void start(){
        assert(not started);
        started = true;
        start_time = std::chrono::steady_clock::now();
    }

    void stop(){
        assert(started);
        started = false;
        total += std::chrono::duration_cast<std::chrono::microseconds>(
            std::chrono::steady_clock::now() - start_time);
        N+=1;
    }
    double average_time_seconds(){
        return total.count()*(1.0e-6/N);
    }
};
extern Profiler prof1, prof2, prof3, prof4, prof5, prof6, prof7;

}
#endif //PROBLEM_MATRICES_HPP